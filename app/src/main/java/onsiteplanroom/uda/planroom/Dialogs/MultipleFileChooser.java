package onsiteplanroom.uda.planroom.Dialogs;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import onsiteplanroom.uda.planroom.UI.FilesListFragment;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/18/2016.
 * This class defines the fragment for the multiple file chooser fragment. This fragment is
 * launched multiple files have been selected and then the user hits the move button. The user can
 * then navigate through the file tree and move a file to the currently selected directory.
 */
public class MultipleFileChooser extends DialogFragment {

    // ******
    // FIELDS
    // ******

    public FilesListFragment parentView;
    private FileChooserAdapter mAdapter;
    private RecyclerView mFileChooserRecyclerView;
    private ImageView mUpButton;
    private LinearLayout mMoveButton;
    private ArrayList<FolderFile> mMovingFolders;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FolderFileLab lab = FolderFileLab.get(getContext());
        mMovingFolders = new ArrayList<>();
        ArrayList<String> stringIds;

        Bundle b = getArguments();
        stringIds = b.getStringArrayList("multiple_files");
        for (String id : stringIds) {
            FolderFile file = lab.getFolderFile(Integer.parseInt(id));
            mMovingFolders.add(file);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_chooser_recycler, container, false);

        mFileChooserRecyclerView = (RecyclerView)
                view.findViewById(R.id.file_chooser_recycler_view_custom_header);
        mFileChooserRecyclerView.setLayoutManager(new
                LinearLayoutManager(getActivity()));

        mUpButton = (ImageView) view.findViewById(R.id.file_chooser_up_arrow);
        mUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer newParent;
                FolderFileLab lab = FolderFileLab.get(getContext());
                Integer currentParent = lab.getParentCurrent();
                if (currentParent != -1) {
                    newParent = lab.getFolderFile(currentParent).getParent();
                    lab.setParentCurrent(newParent);
                    updateUI();
                }

            }
        });

        mMoveButton = (LinearLayout) view.findViewById(R.id.file_chooser_options_menu_bottom);
        mMoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FolderFileLab lab = FolderFileLab.get(getContext());
                for (FolderFile file : mMovingFolders) {
                    mMoveButton.setBackgroundColor(Color.parseColor("#4682B4"));

                    Integer newParentId = lab.getParentCurrent();
                    Integer oldParentId = file.getParent();

                    if (newParentId == -1) {
                        Toast.makeText(getContext(), "Can't move files to project directory",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        FolderFile oldParentFile = lab.getFolderFile(oldParentId);
                        oldParentFile.removeChild(file.getId());
                        lab.updateFolderFile(oldParentFile);

                        FolderFile newFile = lab.getFolderFile(newParentId);
                        newFile.addChild(file.getId());
                        lab.updateFolderFile(newFile);

                        file.setParent(lab.getParentCurrent());
                        lab.updateFolderFile(file);
                    }
                }
                getDialog().dismiss();
                parentView.updateUI();
            }
        });

        getDialog().setTitle("Choose Directory");
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateUI();
        return view;
    }

    // *******
    // METHODS
    // *******

    private void updateUI() {
        List<FolderFile> files = FolderFileLab.get(getContext()).getFolderFiles();
        Collections.sort(files);

        mAdapter = null;
        mAdapter = new FileChooserAdapter(files);
        mFileChooserRecyclerView.setAdapter(mAdapter);
    }

    // *************
    // Inner Classes
    // *************

    private class MultipleFileChooserHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        // ******
        // FIELDS
        // ******

        private FolderFile mFolderFile;
        private TextView mSubjectTextView;

        // ***********
        // Constructor
        // ***********

        public MultipleFileChooserHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mSubjectTextView = (TextView) itemView
                    .findViewById(R.id.list_item_file_chooser_subject_textview);
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public void onClick(View v) {

            List<Integer> ids = new ArrayList<>();

            for (FolderFile file : mMovingFolders) {
                ids.add(file.getId());
            }
            if (mFolderFile.isPhoto()) {
                Toast.makeText(getContext()
                        , "You can't move anything into a file", Toast.LENGTH_SHORT).show();
            } else if (ids.contains(mFolderFile.getId())) {
                Toast.makeText(getContext(), "You can't move a folder into one of it's sub-folders.",
                        Toast.LENGTH_SHORT).show();
            } else {
                FolderFileLab.get(getContext()).setParentCurrent(mFolderFile.getId());
                updateUI();
            }

        }

        // *******
        // METHODS
        // *******

        public void bindMultipleFileChooser(FolderFile file) {
            mFolderFile = file;

            mSubjectTextView.setText(mFolderFile.getName());
        }
    }

    private class FileChooserAdapter extends RecyclerView.Adapter<MultipleFileChooserHolder> {

        // ******
        // FIELDS
        // ******

        private List<FolderFile> mFiles;
        private List<FolderFile> tempFiles = new ArrayList<>();

        // ***********
        // Constructor
        // ***********

        public FileChooserAdapter(List<FolderFile> files) {
            FolderFileLab lab = FolderFileLab.get(getContext());
            mFiles = files;

            if (lab.getParentCurrent() == null) {
                for (FolderFile file : mFiles) {
                    if (file.isProject() || file.getParent() == null) {
                        tempFiles.add(file);
                    }
                }
            } else {
                if (lab.getFolderFile(lab.getParentCurrent())
                        .getChildren() == null || lab.getFolderFile(lab.getParentCurrent())
                        .getChildren().size() < 1) {
                    mFiles = null;
                } else {
                    for (FolderFile file : mFiles) {

                        if (lab.getFolderFile(lab.getParentCurrent())
                                .getChildren().contains(file.getId())) {

                            tempFiles.add(file);
                        }
                    }
                }
            }
            mFiles = tempFiles;
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public MultipleFileChooserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_file_chooser, parent, false);
            ImageView preview = (ImageView) view.findViewById(R.id.file_chooser_preview_image);
            switch (viewType) {
                case 0:
                    Drawable drawable = getResources().getDrawable(R.mipmap.project_blu);
                    preview.setBackground(drawable);
                    break;
                case 1:

                    Drawable drawableFile = getResources().getDrawable(R.mipmap.picture_blu);
                    preview.setBackground(drawableFile);
                    break;
                case 3:
                    Drawable drawableVideo = getResources().getDrawable(R.mipmap.videocam);
                    preview.setBackground(drawableVideo);
                    break;
                default:
                    //
            }

            return new MultipleFileChooserHolder(view);
        }

        @Override
        public int getItemViewType(int position) {
            FolderFile file = mFiles.get(position);

            //Project
            if (file.isProject()) {
                return 0;

                // Photo
            } else if (file.isPhoto()) {
                return 1;

                //Video
            } else if (file.isVideo()) {
                return 3;

                //Folder
            } else
                return 2;
        }

        @Override
        public void onBindViewHolder(MultipleFileChooserHolder holder, int position) {
            FolderFile file = mFiles.get(position);

            holder.bindMultipleFileChooser(file);
        }

        @Override
        public int getItemCount() {
            return mFiles.size();
        }
    }
}