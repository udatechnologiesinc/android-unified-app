package onsiteplanroom.uda.planroom.Dialogs;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import onsiteplanroom.uda.planroom.UI.FilesListFragment;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/18/2016.
 */
public class FileChooser extends DialogFragment {

    // ******
    // FIELDS
    // ******

    public FilesListFragment parentView;
    private FileChooserAdapter mAdapter;
    private RecyclerView mFileChooserRecyclerView;
    private ImageView mUpButton;
    private LinearLayout mMoveButton;
    private FolderFile mMovingFolder;
    private Integer mOldParentId;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        mMovingFolder = FolderFileLab.get(getContext())
                .getFolderFile((Integer) b.getSerializable("id_to_chooser"));
        mOldParentId = mMovingFolder.getParent();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.file_chooser_recycler, container, false);

        mFileChooserRecyclerView = (RecyclerView)
                view.findViewById(R.id.file_chooser_recycler_view_custom_header);
        mFileChooserRecyclerView.setLayoutManager(new
                LinearLayoutManager(getActivity()));

        mUpButton = (ImageView) view.findViewById(R.id.file_chooser_up_arrow);
        mUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer newParent;
                FolderFileLab lab = FolderFileLab.get(getContext());
                Integer currentParent = lab.getParentCurrent();
                if (currentParent != -1) {
                    newParent = lab.getFolderFile(currentParent).getParent();
                    lab.setParentCurrent(newParent);
                    updateUI();
                }

            }
        });

        mMoveButton = (LinearLayout) view.findViewById(R.id.file_chooser_options_menu_bottom);
        mMoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FolderFileLab lab = FolderFileLab.get(getContext());
                Integer currentParentId = lab.getParentCurrent();

                if (currentParentId != -1) {

                    mMoveButton.setBackgroundColor(Color.parseColor("#4682B4"));

                    FolderFile newParent = lab.getFolderFile(currentParentId);
                    FolderFile oldParent = lab.getFolderFile(mOldParentId);

                    oldParent.removeChild(mMovingFolder.getId());
                    lab.updateFolderFile(oldParent);
                    newParent.addChild(mMovingFolder.getId());
                    lab.updateFolderFile(newParent);

                    mMovingFolder.setParent(newParent.getId());
                    lab.updateFolderFile(mMovingFolder);
                    getDialog().dismiss();
                    parentView.updateUI();
                } else {
                    Toast.makeText(getContext(), "Can't move to project " +
                            "directory", Toast.LENGTH_SHORT).show();
                }
            }

        });

        getDialog().setTitle("Choose Directory");
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateUI();
        return view;
    }

    // *******
    // METHODS
    // *******

    private void updateUI() {
        List<FolderFile> files = FolderFileLab.get(getContext()).getFolderFiles();
        Collections.sort(files);

        mAdapter = null;
        mAdapter = new FileChooserAdapter(files);
        mFileChooserRecyclerView.setAdapter(mAdapter);
    }

    // *************
    // Inner Classes
    // *************

    private class FileChooserHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        // ******
        // FIELDS
        // ******

        private FolderFile mFolderFile;
        private TextView mSubjectTextView;

        // ***********
        // Constructor
        // ***********

        public FileChooserHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mSubjectTextView = (TextView) itemView
                    .findViewById(R.id.list_item_file_chooser_subject_textview);
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public void onClick(View v) {
            if (mMovingFolder.getId().equals(mFolderFile.getId())) {
                Toast.makeText(getContext(), "You can't move a folder into one of it's sub-folders.",
                        Toast.LENGTH_SHORT).show();
            } else {
                FolderFileLab.get(getContext()).setParentCurrent(mFolderFile.getId());
                updateUI();
            }
        }

        // *******
        // METHODS
        // *******

        public void bindFileChooser(FolderFile file) {
            mFolderFile = file;

            mSubjectTextView.setText(mFolderFile.getName());
        }
    }

    private class FileChooserAdapter extends RecyclerView.Adapter<FileChooserHolder> {

        // ******
        // FIELDS
        // ******

        private List<FolderFile> mFiles;
        private List<FolderFile> tempFiles = new ArrayList<>();

        // ***********
        // Constructor
        // ***********

        public FileChooserAdapter(List<FolderFile> files) {
            FolderFileLab lab = FolderFileLab.get(getContext());
            mFiles = files;

            if (lab.getParentCurrent() == null) {
                for (FolderFile file : mFiles) {
                    if (file.isProject() || file.getParent() == null) {
                        tempFiles.add(file);
                    }
                }
            } else {
                if (lab.getFolderFile(lab.getParentCurrent())
                        .getChildren() == null || lab.getFolderFile(lab.getParentCurrent())
                        .getChildren().size() < 1) {
                    mFiles = null;
                } else {
                    for (FolderFile file : mFiles) {

                        if (lab.getFolderFile(lab.getParentCurrent())
                                .getChildren().contains(file.getId())) {

                            tempFiles.add(file);
                        }
                    }
                }
            }
            mFiles = tempFiles;
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public FileChooserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_file_chooser, parent, false);
            ImageView preview = (ImageView) view.findViewById(R.id.file_chooser_preview_image);
            switch (viewType) {
                case 0:
                    Drawable drawable = getResources().getDrawable(R.mipmap.project_blu);
                    preview.setBackground(drawable);
                    break;
                case 1:

                    Drawable drawableFile = getResources().getDrawable(R.mipmap.picture_blu);
                    preview.setBackground(drawableFile);
                    break;
                case 3:
                    Drawable drawableVideo = getResources().getDrawable(R.mipmap.videocam);
                    preview.setBackground(drawableVideo);
                    break;
                default:
                    //
            }

            return new FileChooserHolder(view);
        }

        @Override
        public int getItemViewType(int position) {
            FolderFile file = mFiles.get(position);

            //Project
            if (file.isProject()) {
                return 0;

                // File
            } else if (file.isPhoto()) {
                return 1;
            } else if (file.isVideo()) {
                return 3;

                //Folder
            } else
                return 2;
        }

        @Override
        public void onBindViewHolder(FileChooserHolder holder, int position) {
            FolderFile file = mFiles.get(position);

            holder.bindFileChooser(file);
        }

        @Override
        public int getItemCount() {
            return mFiles.size();
        }
    }
}