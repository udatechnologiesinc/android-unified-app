package onsiteplanroom.uda.planroom.Details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/14/2016.
 * This is the class that defines what data goes into the ViewPager.
 */
public class ImageViewPager extends FragmentActivity {

    // ******
    // FIELDS
    // ******

    private ViewPager mViewPager;
    private List<FolderFile> mFolderFiles;
    private Context mContext = this;
    private static final String EXTRA_FOLDERFILE_ID_PAGER = "folder_file_id_from_viewpager";

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_view_pager);

        Integer folderFileId = (Integer) getIntent().getSerializableExtra(EXTRA_FOLDERFILE_ID_PAGER);

        mViewPager = (ViewPager) findViewById(R.id.picture_detail_pager);
        FragmentManager fm = getSupportFragmentManager();

        FolderFileLab lab = FolderFileLab.get(mContext);

        // Potential problem with null parent.

        List<Integer> ids = lab.getFolderFile(lab.getParentCurrent()).getChildren();
        mFolderFiles = new ArrayList<>();

        for (Integer id : ids) {
            if (lab.getFolderFile(id).isVideo() ||
                    lab.getFolderFile(id).isPhoto()) {
                mFolderFiles.add(lab.getFolderFile(id));
            }
        }

        // Potential problem with less than 3 folders
        //Collections.sort(mFolderFiles);

        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                FolderFile folderFile = mFolderFiles.get(position);
                return MediaDetailFragment.newInstance(folderFile.getId());
            }

            @Override
            public int getCount() {
                return mFolderFiles.size();
            }
        });

        for (int i = 0; i < mFolderFiles.size(); i++) {
            if (mFolderFiles.get(i).getId().equals(folderFileId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    // *******
    // METHODS
    // *******

    /**
     * Creates an intent with the current folderfile and sends it to the ImageViewPager.class.
     *
     * @param packageContext
     * @param itemId         - FolderFile COLid
     * @return an intent.
     */

    public static Intent newIntent(Context packageContext, Integer itemId) {
        Intent intent = new Intent(packageContext, ImageViewPager.class);
        intent.putExtra(EXTRA_FOLDERFILE_ID_PAGER, itemId);
        return intent;
    }
}
