package onsiteplanroom.uda.planroom.Details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.UI.MainActivity;
import onsiteplanroom.uda.planroom.labs.ContactLab;

/**
 * Created by Harry on 2/16/2016.
 * This class allows for editing the contacts.
 */
public class EditContactFragment extends Fragment {

    // ******
    // FIELDS
    // ******

    private Contact mContact;
    private static final String RETURN_FRAGMENT = "return_fragment";
    private ActionBar mActionBar;
    private EditText mFirstName;
    private EditText mMiddleName;
    private EditText mLastName;
    private EditText mEmail;
    private EditText mDisplayName;
    private EditText mCompany;
    private EditText mAddress;
    private EditText mCity;
    private EditText mState;
    private EditText mZip;
    private EditText mPhone;
    private EditText mMobilePhone;
    private EditText mMobileProvider;
    private EditText mFax;
    private EditText mWebsite;
    private EditText mSummary;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_contact_details, container, false);

        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(true);
        }

        mFirstName = (EditText)view.findViewById(R.id.first_name_contact);
        mFirstName.setText(mContact.getFirstName());
        mFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mMiddleName = (EditText)view.findViewById(R.id.middle_name_contact);
        mMiddleName.setText(mContact.getMiddleName());

        mLastName = (EditText)view.findViewById(R.id.last_name_contact);
        mLastName.setText(mContact.getLastName());


        mEmail = (EditText)view.findViewById(R.id.email_contact);
        mEmail.setText(mContact.getEmail());


        mDisplayName = (EditText)view.findViewById(R.id.display_name_contact);
        mDisplayName.setText(mContact.getDisplayName());


        mCompany  = (EditText)view.findViewById(R.id.company_contact);
        mCompany.setText(mContact.getCompany());


        mAddress = (EditText)view.findViewById(R.id.address_contact);
        mAddress.setText(mContact.getAddress());


        mCity = (EditText)view.findViewById(R.id.city_contact);
        mCity.setText(mContact.getCity());


        mState = (EditText)view.findViewById(R.id.state_contact);
        mState.setText(mContact.getState());


        mZip = (EditText)view.findViewById(R.id.zip_contact);
        mZip.setText(mContact.getZip());


        mPhone = (EditText)view.findViewById(R.id.phone_contact);
        mPhone.setText(mContact.getPhone());


        mMobilePhone = (EditText)view.findViewById(R.id.mobile_phone_contact);
        mMobilePhone.setText(mContact.getMobilePhone());


        mMobileProvider = (EditText)view.findViewById(R.id.mobile_provider_contact);
        mMobileProvider.setText(mContact.getMobileProvider());

        mFax  = (EditText)view.findViewById(R.id.fax_contact);
        mFax.setText(mContact.getFax());


        mWebsite  = (EditText)view.findViewById(R.id.website_contact);
        mWebsite.setText(mContact.getWebsite());

        mSummary = (EditText)view.findViewById(R.id.summary_contact);
        mSummary.setText(mContact.getSummary());




        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Integer id = (Integer) getActivity().getIntent().getSerializableExtra("id_contact_detail");

        if (id == null) {
            mContact = new Contact();
            ContactLab.get(getContext()).addContact(mContact);
        } else {
            mContact = ContactLab.get(getContext()).getContact(id);
        }
        getActivity().setTitle(mContact.getFirstName() + mContact.getLastName());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_contact_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_contact_delete:

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ContactLab.get(getContext()).deleteContact(mContact);
                                Intent intent = new Intent(getContext(), MainActivity.class);

                                //1 = updates
                                //3 = files
                                //3 = contacts
                                intent.putExtra(RETURN_FRAGMENT, 3);
                                getActivity().finish();
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
                return true;
            case R.id.menu_edit_contact_save:
                Intent intent2 = new Intent(getActivity(), MainActivity.class);
                intent2.putExtra(RETURN_FRAGMENT, 3);
                getActivity().finish();
                startActivity(intent2);
                return true;
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //Makes the name of the contact new contact if nothing was written in the text field.
        if (mContact.getFirstName() == null || mContact.getFirstName().length() < 1) {
            mContact.setFirstName("New Contact");
            ContactLab.get(getContext()).updateContact(mContact);
        }

    }


}
