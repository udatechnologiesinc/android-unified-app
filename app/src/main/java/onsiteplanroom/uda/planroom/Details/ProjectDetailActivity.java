package onsiteplanroom.uda.planroom.Details;

import onsiteplanroom.uda.planroom.UI.SingleFragmentActivity;

/**
 * Created by Harry on 2/15/2016.
 * This class hosts the detail fragment for a project.
 */
public class ProjectDetailActivity extends SingleFragmentActivity {

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new ProjectDetailFragment();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

}
