package onsiteplanroom.uda.planroom.Details;

import onsiteplanroom.uda.planroom.UI.SingleFragmentActivity;

/**
 * Created by Harry on 2/9/2016.
 * This class hosts the detail fragment for contacts.
 */
public class ContactDetailActivity extends SingleFragmentActivity {

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new ContactDetailFragment();
    }



}
