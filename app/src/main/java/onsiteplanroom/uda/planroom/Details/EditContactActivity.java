package onsiteplanroom.uda.planroom.Details;

import onsiteplanroom.uda.planroom.UI.SingleFragmentActivity;

/**
 * Created by Harry on 2/16/2016.
 * This class hosts the edit contact fragment.
 */
public class EditContactActivity extends SingleFragmentActivity {

    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new EditContactFragment();
    }



}
