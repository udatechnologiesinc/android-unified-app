package onsiteplanroom.uda.planroom.Details;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/14/2016.
 * Defines the view for a particular image in the ViewPager. A new istance is called whenever the user
 * swipes right or left to the next or previous photo.
 */
public class MediaDetailFragment extends Fragment {

    private static final String ARG_FOLDERFILE_ID = "arg_folderfile_id";
    private FolderFile mFolderFile;
    private ImageView mImageView;
    private ImageView mPlaceholder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FolderFileLab lab = FolderFileLab.get(getContext());
        Integer id = (Integer) getArguments().getSerializable(ARG_FOLDERFILE_ID);
        mFolderFile = lab.getFolderFile(id);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.image_detail, container, false);
        mImageView = (ImageView) v.findViewById(R.id.image_detail_imageview);
        mPlaceholder = (ImageView) v.findViewById(R.id.progress_circle_image_view);

        if (mFolderFile.isPhoto()) {

            try {
                Picasso
                        .with(getContext())
                        .load("http://files.constructiononline.com/picture/LARGE/" +
                                mFolderFile.getId() + "/")
                        .into(mImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                mPlaceholder.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {

                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return null;
        }
        return v;
    }

    /**
     * Creates newInstances of the fragnment for each position in the ViewPager.
     *
     * @param id - COLid of the current folderfile.
     * @return an isntance of the current fragment representing a slide in thhe gallery.
     */

    public static MediaDetailFragment newInstance(Integer id) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_FOLDERFILE_ID, id);
        MediaDetailFragment fragment = new MediaDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }
}
