package onsiteplanroom.uda.planroom.Details;

import onsiteplanroom.uda.planroom.UI.SingleFragmentActivity;

/**
 * Created by Harry on 2/16/2016.
 * This class hosts the edit project fragment.
 */
public class EditProjectActivity extends SingleFragmentActivity {


    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new EditProjectFragment();
    }



}
