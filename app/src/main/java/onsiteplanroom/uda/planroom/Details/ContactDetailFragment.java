package onsiteplanroom.uda.planroom.Details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import onsiteplanroom.uda.planroom.UI.MainActivity;
import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.ContactLab;

/**
 * Created by Harry on 2/9/2016.
 * This class has the detail information for each contact. You can edit this fragment
 * by clicking on the top right icon.
 */
public class ContactDetailFragment extends Fragment {

    // ******
    // FIELDS
    // ******

    private Contact mContact;
    private static final String CONTACT_ID_FROM_DETAIL = "id_contact_detail";
    private static final String RETURN_FRAGMENT_CONTACT_DETAIL = "return_fragment_contact_detail";
    private TextView mContactNameView;
    private ActionBar mActionBar;
    private TextView mCompanyName;
    private TextView mDisplayName;
    private TextView mAddress;
    private TextView mEmail;
    private TextView mPrimaryPhone;
    private TextView mMobilePhone;
    private TextView mFax;
    private TextView mWebsite;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.contact_detail, container, false);

        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(true);
        }

        mContactNameView = (TextView) v.findViewById(R.id.contact_name_contact_detail);
        mContactNameView.setText(mContact.getFirstName() + " " +  mContact.getLastName());

        mCompanyName = (TextView)v.findViewById(R.id.contact_company_contact_detail);

        if (!mContact.getCompany().equals("null")) {
            if (mContact.getCompany().length() > 0) {
                mCompanyName.setText(mContact.getCompany());
            }
        }

        mDisplayName = (TextView)v.findViewById(R.id.display_name_contact_detail);

        if (!mContact.getDisplayName().equals("null")) {
            if (mContact.getDisplayName().length() > 0) {

                mDisplayName.setText(mContact.getDisplayName());
            }
        }
        mAddress = (TextView)v.findViewById(R.id.address_contact_detail);

        if (!mContact.getAddress().equals("null")) {
            if (mContact.getAddress().length() > 0) {

                mAddress.setText(mContact.getAddress());
            }
        }

        mPrimaryPhone = (TextView)v.findViewById(R.id.primary_phone_contact_detail);

        if (!mContact.getPhone().equals("null")) {
            if (mContact.getPhone().length() > 0) {

                mPrimaryPhone.setText(mContact.getPhone());
            }
        }

        mMobilePhone = (TextView)v.findViewById(R.id.mobile_phone_contact_detail);
        if (!mContact.getMobilePhone().equals("null")) {
            if (mContact.getMobilePhone().length() > 0) {

                mMobilePhone.setText(mContact.getMobilePhone());
            }
        }

        mFax = (TextView)v.findViewById(R.id.fax_contact_detail);
        if (!mContact.getFax().equals("null")) {
            if (mContact.getFax().length() > 0) {

                mFax.setText(mContact.getFax());
            }
        }

        mWebsite = (TextView)v.findViewById(R.id.website_contact_detail);
        if (!mContact.getWebsite().equals("null")) {
            if (mContact.getWebsite().length() > 0) {

                mWebsite.setText(mContact.getWebsite());
            }
        }

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Intent passed from the ContactsListFragment.class to determine which contact to
        //display info for.
        Integer id = (Integer) getActivity().getIntent().getSerializableExtra("contact_id");
        Integer updateId = (Integer) getActivity().getIntent().getSerializableExtra("update_contact_id");

        if (id != null) {
            mContact = ContactLab.get(getContext()).getContact(id);
        } else {
            mContact = ContactLab.get(getContext()).getContact(updateId);
        }
        getActivity().setTitle(mContact.getFirstName() + " " + mContact.getLastName());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.contact_detail_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //By clicking this button it will launch a fragment that will allow you to edit
            //stuff that can be displayed in this fragment.
            case R.id.menu_contact_detail_edit:
                Intent intent = new Intent(getContext(), EditContactActivity.class);
                intent.putExtra(CONTACT_ID_FROM_DETAIL, mContact.getId());
                startActivity(intent);
                getActivity().finish();
                return true;

            //This deletes the contact and returns you to the list fragments.
            case R.id.menu_contact_detail_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ContactLab.get(getContext()).deleteContact(mContact);
                                Intent intent2 = new Intent(getContext(), MainActivity.class);
                                intent2.putExtra(RETURN_FRAGMENT_CONTACT_DETAIL, 3);
                                startActivity(intent2);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return true;
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return true;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }
}
