package onsiteplanroom.uda.planroom.Details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Date;

import onsiteplanroom.uda.planroom.UI.MainActivity;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/16/2016.
 * This class allows the user to change their project data and add a project.
 */
public class EditProjectFragment extends Fragment {

    // ******
    // FIELDS
    // ******

    EditText mEditProjectName;
    EditText mStreet;
    EditText mCity;
    EditText mState;
    EditText mPostalCode;
    EditText mProjectNumber;
    EditText mProjectType;
    EditText mCompanyName;
    EditText mScope;
    EditText mTime;
    EditText mMoney;
    EditText mNotes;
    FolderFile mFolderFile;
    private static final String RECEIVE_EDIT_ID = "receive_edit_id";
    ActionBar mActionBar;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Integer id = (Integer) getActivity().getIntent().getSerializableExtra("send_id");

        if (id == null) {
            FolderFileLab lab = FolderFileLab.get(getContext());
            mFolderFile = new FolderFile();
            mFolderFile.setParent(-1);
            mFolderFile.setIsProject(true);
            lab.addFolderFile(mFolderFile);
        } else {
            mFolderFile = FolderFileLab.get(getContext()).getFolderFile(id);
        }
        getActivity().setTitle(mFolderFile.getName());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_projecet_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_project_delete:

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FolderFileLab.get(getContext())
                                        .deleteFolderFile(mFolderFile);
                                Intent intent2 = new Intent(getContext(), MainActivity.class);
                                startActivity(intent2);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
                return true;
            case R.id.menu_edit_project_save:
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                return true;
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_project_details, container, false);

        mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(true);
        }

        mEditProjectName = (EditText) view.findViewById(R.id.edit_name);
        mEditProjectName.setText(mFolderFile.getName());
        mEditProjectName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mFolderFile.setName(s.toString());
                mFolderFile.setModifiedDate(new Date());
                FolderFileLab.get(getContext()).updateFolderFile(mFolderFile);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mStreet = (EditText)view.findViewById(R.id.street_address_edit);
        mStreet.setText(mFolderFile.getAddress());


        mCity = (EditText)view.findViewById(R.id.city_edit);
        mCity.setText(mFolderFile.getCity());

        mState = (EditText)view.findViewById(R.id.state_edit);
        mState.setText(mFolderFile.getState());

        mPostalCode = (EditText)view.findViewById(R.id.edit_postal_code);
        mPostalCode.setText(mFolderFile.getZip());

        mProjectNumber = (EditText)view.findViewById(R.id.project_number_edit_project);
        mProjectNumber.setText(mFolderFile.getProjectNumber());

        mProjectType = (EditText)view.findViewById(R.id.project_type_edit_project);
        mProjectType.setText(mFolderFile.getProjectType());

        mCompanyName = (EditText)view.findViewById(R.id.company_edit_project);
        mCompanyName.setText(mFolderFile.getCompanyName());

        mScope = (EditText)view.findViewById(R.id.scope_edit_project);
        mScope.setText(mFolderFile.getScopeOfWork());

        mTime = (EditText)view.findViewById(R.id.time_edit_project);
        mTime.setText("" + mFolderFile.getTotalTime());

        mMoney = (EditText)view.findViewById(R.id.money_edit_project);
        mMoney.setText("" + mFolderFile.getTotalMoney());

        mNotes = (EditText)view.findViewById(R.id.notes_edit_project);
        mNotes.setText(mFolderFile.getProjectNotes());


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        // Error check to keep the app from crashing if no name was written.
        if (mFolderFile.getName() == null || mFolderFile.getName().length() < 1) {
            mFolderFile.setName("New Project");
            FolderFileLab.get(getContext()).updateFolderFile(mFolderFile);
        }
    }


}
