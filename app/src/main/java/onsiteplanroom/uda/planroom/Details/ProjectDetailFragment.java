package onsiteplanroom.uda.planroom.Details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.UI.MainActivity;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/15/2016.
 * This is the fragment that defines the detail of the project object.
 * This fragment appears when the info button is pressed on the list in FilesListFragment
 */
public class ProjectDetailFragment extends Fragment {

    // ******
    // FIELDS
    // ******

    private FolderFile mFolderFile;
    private TextView mCompanyName;
    private TextView mCreator;
    private TextView mAddress;
    private TextView mType;
    private TextView mNumber;
    private TextView mMoney;
    private TextView mTime;
    private TextView mProjectNameView;
    private static final String SEND_ID = "send_id";
    private ActionBar mActionBar;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.project_detail, container, false);


        mProjectNameView = (TextView) view.findViewById(R.id.project_name_project_detail);
        mProjectNameView.setText(mFolderFile.getName());

        if (mFolderFile.getCompanyName().equals("null") || mFolderFile.getCompanyName().length() < 1) {
            // Do nothing
        } else {
            mCompanyName = (TextView) view.findViewById(R.id.company_name_project_detail);
            mCompanyName.setText(mFolderFile.getCompanyName());
        }


        if (mFolderFile.getAddress().equals("null") || mFolderFile.getAddress().length() < 1) {
        } else {
            mCreator = (TextView)view.findViewById(R.id.creator_name_project_detail);
            mCreator.setText("Created by: " + mFolderFile.getCreatorName());
        }

        if (mFolderFile.getAddress().equals("null") || mFolderFile.getAddress().length() < 1) {

        } else {
            mAddress = (TextView) view.findViewById(R.id.address_project_detail);
            mAddress.setText(mFolderFile.getAddress());
        }

        if (mFolderFile.getProjectType().equals("null") || mFolderFile.getProjectType().length() < 1) {

        } else {
            mType = (TextView) view.findViewById(R.id.type_project_detail);
            mType.setText(mFolderFile.getProjectType());
        }

        if (mFolderFile.getProjectType().equals("null") || mFolderFile.getProjectType().length() < 1) {
        } else {
            mNumber = (TextView) view.findViewById(R.id.number_project_detail);
            mNumber.setText(mFolderFile.getProjectNumber());
        }

        if (mFolderFile.getProjectType().equals("null") || mFolderFile.getProjectType().length() < 1) {
        } else {
            mMoney = (TextView) view.findViewById(R.id.cost_detail_project);
            mMoney.setText("" + mFolderFile.getTotalMoney());
        }

        if (mFolderFile.getProjectType().equals("null") || mFolderFile.getProjectType().length() < 1) {

        } else {
            mTime = (TextView) view.findViewById(R.id.time_detail_project);
            mTime.setText("" + mFolderFile.getTotalTime());
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String id = getActivity().getIntent().getStringExtra("id_project");
        Integer updateId =
                (Integer) getActivity().getIntent().getSerializableExtra("update_project_id");

        if (id != null) {
            mFolderFile = FolderFileLab.get(getContext()).getFolderFile(Integer.parseInt(id));
        } else {
            mFolderFile = FolderFileLab.get(getContext()).getFolderFile(updateId);
        }
        getActivity().setTitle(mFolderFile.getName());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.project_detail_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_project_detail_edit:
                Intent intent = new Intent(getContext(), EditProjectActivity.class);
                intent.putExtra(SEND_ID, mFolderFile.getId());
                startActivity(intent);
                getActivity().finish();
                return true;
            case R.id.menu_project_detail_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure you want to delete this?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                FolderFileLab.get(getContext())
                                        .deleteFolderWithChildren(mFolderFile.getId());
                                FolderFileLab.get(getContext())
                                        .deleteFolderFile(mFolderFile);
                                Intent intent2 = new Intent(getContext(), MainActivity.class);
                                startActivity(intent2);
                                getActivity().finish();
                            }
                        })
                        .show();
                return true;
            case android.R.id.home:
                getActivity().finish();
                return true;
            default:
                return true;
        }
    }
}
