package onsiteplanroom.uda.planroom.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.WebServices.InitialDownload;
import onsiteplanroom.uda.planroom.labs.ContactLab;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/14/2016.
 * This is the Fragment that is used to get the email and password from the user. After it
 * retrieves this information the app calls all the services and then loads all the data into the
 * app.
 */
public class LoginFragment extends Fragment {

    // ******
    // FIELDS
    // ******

    private Button mLoginButton;
    private EditText mEmailEdit;
    private EditText mPasswordEdit;
    private static final String USER_PREF = "user_pref";
    private static final String PASSWORD_PREF = "password_pref";
    private static final String FIRST_RUN = "first_run";
    private static final String FILES_LAST_REFRESHED = "files_last_refreshed";
    private static final String CONTACTS_LAST_REFRESHED = "contacts_last_refreshed";
    private String mUser;
    private String mPassword;
    private boolean firstRun;
    private TextView testText;

    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);

        // This is used to check if it is the first run ever of the app. This is necessary because
        // I need to know if I should create the database or just make a new table for the data.
        boolean isFirstRun = prefs.getBoolean(FIRST_RUN, true);

        if (isFirstRun) {
            prefs.edit().putBoolean(FIRST_RUN, false).apply();
            firstRun = true;
        } else {
            firstRun = false;
        }

        // If the user already logged in and did not log out then skip this fragment.
        String savedUser = prefs.getString(USER_PREF, null);
        if (savedUser != null && savedUser.length() > 0) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);
            getActivity().finish();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_screen, container, false);


        // Sets the password equal to @systems12 right now for testing purposes
        mPasswordEdit = (EditText) view.findViewById(R.id.password_edit_login);
        mPasswordEdit.setText("@systems12");
        mPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mEmailEdit = (EditText) view.findViewById(R.id.email_edit_login);
        mEmailEdit.setText("hsummers@uda1.com");
        mEmailEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // When the login button is pressed the downloadData() is called which starts the
        // download chain. In the mean time it displays a download spinner.
        mLoginButton = (Button) view.findViewById(R.id.login_button_login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle("Downloading Projects");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                FolderFileLab.get(getContext()).setParentCurrent(-1);

                if (!firstRun) {
                    FolderFileLab.get(getContext()).initializeFolderFileTable();
                    ContactLab.get(getContext()).initializeContactTable();

                    SharedPreferences prefs = getActivity().getSharedPreferences(USER_PREF,
                            Context.MODE_PRIVATE);
                    prefs.edit().putString(USER_PREF, mEmailEdit.getText().toString()).apply();
                    prefs.edit().putString(PASSWORD_PREF, mPasswordEdit.getText().toString()).apply();

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");
                    String nowDate = df.format(new Date());

                    prefs.edit().putString(FILES_LAST_REFRESHED, nowDate).apply();
                    prefs.edit().putString(CONTACTS_LAST_REFRESHED, nowDate).apply();
                } else {
                    firstRun = false;
                }

                SharedPreferences prefs = getActivity().getSharedPreferences(USER_PREF,
                        Context.MODE_PRIVATE);

                prefs.edit().putString(USER_PREF, mEmailEdit.getText().toString()).apply();
                prefs.edit().putString(PASSWORD_PREF, mPasswordEdit.getText().toString()).apply();

                downloadData();
            }

        });

        return view;
    }

    // *******
    // METHODS
    // *******

    /**
     * This method starts the chain for downloading all the data into the app. The order of download
     * is contacts > Projects > Files. The next service is called in the onPostExecute() of the
     * previous service.
     */

    private void downloadData() {
        // The user and password is retrieved from shared preferences
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "user_pref", Context.MODE_PRIVATE);
        mUser = prefs.getString("user_pref", null);
        mPassword = prefs.getString("password_pref", null);

        // First webservice is called here.
        InitialDownload.downloadAll(getContext(), mUser, mPassword);
    }


}
