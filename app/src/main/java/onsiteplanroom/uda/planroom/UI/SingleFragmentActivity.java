package onsiteplanroom.uda.planroom.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import onsiteplanroom.uda.planroom.R;

/**
 * This class is what I use to make simple 1 fragment activities.
 * This class requires Overriding the createFragment methood if you want to implement it.
 */

public abstract class SingleFragmentActivity extends AppCompatActivity {

    protected abstract android.support.v4.app.Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

        if (fragment == null) {
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }
    }
}
