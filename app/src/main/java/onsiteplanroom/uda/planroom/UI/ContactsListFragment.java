package onsiteplanroom.uda.planroom.UI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import onsiteplanroom.uda.planroom.Details.ContactDetailActivity;
import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.WebServices.RefreshContacts;
import onsiteplanroom.uda.planroom.labs.ContactLab;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/2/2016.
 * This class is hosted by the MainActivity and is one of the 3 main fragments in the app.
 * This app lists out all the contacts in alphabetical order with a recycler view.
 * This class has 2 inner classes. The Holder and the Adapter for the recycler view.
 */
public class ContactsListFragment extends Fragment implements RecyclerView.OnItemTouchListener,
        ActionMode.Callback {

    // ******
    // FIELDS
    // ******

    private RecyclerView mContactRecycler;
    private ContactAdapter mAdapter;
    private ActionMode actionMode;
    private SwipeRefreshLayout mRefreshButton;
    private ContactsListFragment selfFrag = this;


    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.contact_list_menu, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        switch (menuItem.getItemId()) {
            case R.id.contact_logout:
                Intent intent = new Intent(getActivity(),LoginActivity.class);
                startActivity(intent);
                getActivity().finish();

                SharedPreferences prefs = getActivity().getSharedPreferences("user_pref",
                        Context.MODE_PRIVATE);
                prefs.edit().remove("user_pref").apply();

                FolderFileLab.get(getContext()).dropFolderFileTable();
                ContactLab.get(getContext()).dropContactTable();
                return true;
            default:
                return false;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (((AppCompatActivity)getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Contacts");
        }
        updateUI();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // This class inflates the recycler view.
        View view = inflater.inflate(R.layout.contacts_container, container, false);
        mContactRecycler = (RecyclerView) view.findViewById(R.id.contacts_recycler_view);
        mContactRecycler.setLayoutManager(new
                LinearLayoutManager(getActivity()));
        mContactRecycler.addOnItemTouchListener(this);

        mRefreshButton = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_contacts);
        mRefreshButton.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        updateUI();
        setHasOptionsMenu(true);
        return view;
    }

    // Override methods for the multiple select feature.

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        this.actionMode = null;
        mAdapter.clearSelections();
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.selection_contact_delete_icon:
                List<Integer> selectedItemPositions =
                        mAdapter.getSelectedItems();
                List<Contact> contacts = ContactLab.get(getContext()).getContacts();
                Collections.sort(contacts);
                for (int i = 0; i < selectedItemPositions.size(); i++) {
                    ContactLab.get(getContext()).deleteContact(contacts
                            .get(selectedItemPositions.get(i)));
                }
                updateUI();
                return true;
            default:
                return false;
        }
    }


    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.contact_select, menu);
        return true;
    }


    // *******
    // METHODS
    // *******

    /** Used in multiple select mode to change the title as well as toggle the selected
     *            item.
     * @param idx - position of selected item.
     */

    private void myToggleSelection(int idx) {
        mAdapter.toggleSelection(idx);
        String title = getString(R.string.selected_count, mAdapter.getSelectedItemCount());
        actionMode.setTitle(title);

    }

    /**
     * This is used when the refresh button is initiated. It will start the download chain for
     * checking to see if anything has changed with the contacts in col.
     */

    private void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                // Shared Preferences to get user and password then load up contact in database
                SharedPreferences prefs = getActivity().getSharedPreferences(
                        "user_pref", Context.MODE_PRIVATE);
                String user_pref = prefs.getString("user_pref", null);
                String password_pref = prefs.getString("password_pref", null);
                String date_pref = prefs.getString("contacts_last_refreshed", null);

                RefreshContacts.refreshContacts(getContext(), user_pref, password_pref,
                        date_pref, selfFrag, mRefreshButton);

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");
                String nowDate = df.format(new Date());

                prefs.edit().putString("contacts_last_refreshed", nowDate).apply();

            }
        }, 0);
    }

    /**
     * This method updates the view in the recycler view. This is called when there has been
     * a change and you want to update the screen right away. This calls also calls the adapter
     * which formats the list items to look like you want them to.
     */

    public void updateUI() {
        List<Contact> contacts = ContactLab.get(getContext()).getContacts();
        Collections.sort(contacts);
        mAdapter = null;
        mAdapter = new ContactAdapter(contacts);
        mContactRecycler.setAdapter(mAdapter);
    }

    // *************
    // Inner Classes
    // *************

    /**
     * This class is a Holder class. It contains all the code for formatting/ wiring up
     * all the individual list items. It also contains the all important onClick method
     * so that if you click on a list item you can do something with it. In this case it will
     * launch the contact detail fragment.
     */

    private class ContactHolder extends android.support.v7.widget.RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {

        // ******
        // FIELDS
        // ******

        private Contact mContact;
        private TextView mSubjectTextView;
        private TextView mDateModifiedTextView;
        private ImageView mImagePreview;
        private RelativeLayout itemBackground;
        private static final String CONTACT_ID = "contact_id";
        private TextView mLetter;
        private View mSeperator;

        // ***********
        // Constructor
        // ***********

        public ContactHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mSubjectTextView = (TextView) itemView
                    .findViewById(R.id.list_item_contact_subject_textview);
            mLetter = (TextView) itemView.findViewById(R.id.alphabet_text);
            mSeperator = (View) itemView.findViewById(R.id.contact_seperator);
            itemBackground = (RelativeLayout) itemView.findViewById(R.id.contact_list_item_layout);


        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public void onClick(View v) {

            if (actionMode != null) {
                int idx = mContactRecycler.getChildPosition(v);
                myToggleSelection(idx);


                if (itemBackground.getBackground() != null) {
                    itemBackground.setBackground(null);
                } else {
                    itemBackground.setBackgroundColor(Color.parseColor("#B2EBF2"));
                }
            } else {

                Intent intent = new Intent(getContext(), ContactDetailActivity.class);
                intent.putExtra(CONTACT_ID, mContact.getId());
                startActivity(intent);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (actionMode == null) {
                actionMode = getActivity().startActionMode(ContactsListFragment.this);
            }
            itemBackground = (RelativeLayout) v.findViewById(R.id.contact_list_item_layout);


            if (itemBackground.getBackground() != null) {
                itemBackground.setBackground(null);
            } else {
                itemBackground.setBackgroundColor(Color.parseColor("#B2EBF2"));
            }


            int idx = mContactRecycler.getChildPosition(v);
            myToggleSelection(idx);

            return true;
        }


        /**
         * Makes each contact list item unique. This method is called for each item in the list.
         *
         * @param contact - the contact that is being set.
         */

        public void bindContact(Contact contact) {

            List<Contact> contacts = ContactLab.get(getContext()).getContacts();
            Collections.sort(contacts);
            List<Integer> ids = new ArrayList<>();

            for (Contact con: contacts) {
                ids.add(con.getId());
            }
            Integer idCurrentContact = ids.indexOf(contact.getId());

            if (!idCurrentContact.equals(0)) {
                Contact previousContact = ContactLab.get(getContext())
                        .getContact(ids.get(idCurrentContact - 1));
                if (previousContact.getFirstName().toUpperCase().charAt(0)
                        != contact.getFirstName().toUpperCase().charAt(0)) {
                    mLetter.setText("" + contact.getFirstName().toUpperCase().charAt(0));
                    mSeperator.setVisibility(View.VISIBLE);
                    mSubjectTextView.setPadding(0,0,0,0);
                } else {
                    mSubjectTextView.setPadding(0,0,0,10);
                }
            } else {
                mLetter.setText("" + contact.getFirstName().charAt(0));
                mSeperator.setVisibility(View.VISIBLE);

            }

            mContact = contact;
            mSubjectTextView.setText(mContact.getFirstName() + " " + mContact.getLastName());

        }

    }

    /**
     * This is the adapter for the recycler view. The adapter is called from the updateUI method
     * in the outer class. The list of contacts from the database is passed in.
     */

    private class ContactAdapter extends RecyclerView.Adapter<ContactHolder> {

        // ******
        // FIELDS
        // ******

        private List<Contact> mContacts;
        private SparseBooleanArray selectedItems;


        // ***********
        // Constructor
        // ***********

        public ContactAdapter(List<Contact> contacts) {

            Collections.sort(contacts);

            mContacts = contacts;

            selectedItems = new SparseBooleanArray();
        }

        @Override
        public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_contact, parent, false);

            return new ContactHolder(view);
        }

        @Override
        public void onBindViewHolder(ContactHolder holder, int position) {
            Contact contact = mContacts.get(position);
            holder.bindContact(contact);
        }

        @Override
        public int getItemCount() {
            return mContacts.size();
        }

        // *******
        // METHODS
        // *******

        /** Puts the selected item into the boolean Collection designating it as selected.
         * @param pos - position of item that was selected in the list.
         */

        public void toggleSelection(int pos) {
            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
            } else {
                selectedItems.put(pos, true);
            }
        }

        /**
         * Clears the boolean collection of items that have been selected.
         */

        public void clearSelections() {
            selectedItems.clear();
            updateUI();
        }

        /** Gives the size of the selected items boolean Collection.
         * @return number of items selected. Used to change the title with this count.
         */

        public int getSelectedItemCount() {
            return selectedItems.size();
        }

        /** The selected items currently shown.
         * @return the list of ids for the items that have been selected.
         */

        public List<Integer> getSelectedItems() {
            List<Integer> items =
                    new ArrayList<Integer>(selectedItems.size());

            for (int i = 0; i < selectedItems.size(); i++) {
                items.add(selectedItems.keyAt(i));
            }
            return items;
        }
    }
}

