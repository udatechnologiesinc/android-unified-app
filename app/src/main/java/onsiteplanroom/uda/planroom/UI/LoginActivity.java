package onsiteplanroom.uda.planroom.UI;

import android.os.Bundle;

/**
 * Created by Harry on 3/14/2016.
 * This activity hosts the LoginFragment. It also hides the support action bar.
 */
public class LoginActivity extends SingleFragmentActivity {
    @Override
    protected android.support.v4.app.Fragment createFragment() {
        return new LoginFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            getSupportActionBar().hide();

    }

}
