package onsiteplanroom.uda.planroom.UI;

import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.File;

import onsiteplanroom.uda.planroom.Details.EditContactActivity;
import onsiteplanroom.uda.planroom.Details.EditProjectActivity;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.Utilities.TabListener;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/9/2016.
 */
public class MainActivity extends AppCompatActivity {

    private Fragment contactsFragment = new ContactsListFragment();
    private Fragment filesFragment = new FilesListFragment();
    private FloatingActionButton mFab;
    private static final int SELECT_PICTURE = 1;
    private static final int SELECT_VIDEO = 2;
    private final Context context = this;
    private static final int FILE_REQUEST_IMAGE_CAPTURE = 3;
    private static final int FILE_REQUEST_VIDEO_CAPTURE = 4;
    private static final String PHOTOT_FILE_ID = "photo_file_id";
    public static String lastFragment;

    //Draw stuff
    private String[] drawerStrings;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Make the Drawer
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        drawerStrings = getResources().getStringArray(R.array.app_drawer_strings);
        mDrawerLayout = (DrawerLayout) inflater.inflate(R.layout.drawer_layout, null);

//        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
//        View child = decor.getChildAt(0);
//        decor.removeView(child);
//        FrameLayout container = (FrameLayout) mDrawerLayout.findViewById(R.id.content_frame); // This is the container we defined just now.
//        container.addView(child);
//        decor.addView(mDrawerLayout);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }


            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerStrings));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_TABS);


        android.support.v7.app.ActionBar.Tab Tab2 = actionBar.newTab().setText("Files");
        android.support.v7.app.ActionBar.Tab Tab3 = actionBar.newTab().setText("Contacts");

        Tab2.setTabListener(new TabListener(filesFragment));
        Tab3.setTabListener(new TabListener(contactsFragment));


        actionBar.addTab(Tab2);
        actionBar.addTab(Tab3);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Select Action");

                final CharSequence[] items = {"Add Project", "Add Contact",
                        "Upload Picture", "Upload Video", "Take Photo", "Take Video"};
                builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                switch (which) {

                                    // Add Project
                                    case 0:
                                        lastFragment = "files";
                                        Intent intent = new Intent(getApplicationContext(),
                                                EditProjectActivity.class);
                                        startActivity(intent);

                                        break;

                                    // Add Contact
                                    case 1:
                                        lastFragment = "contacts";
                                        Intent intent2 = new Intent(getApplicationContext(),
                                                EditContactActivity.class);
                                        startActivity(intent2);

                                        break;

                                    // Upload Picture
                                    case 2:

                                        if (FolderFileLab.get(context).getParentCurrent() == -1) {
                                            Toast.makeText(context, "Can't add photo to project directory",
                                                    Toast.LENGTH_SHORT).show();
                                            break;
                                        } else {


                                            lastFragment = "files";
                                            FolderFileLab lab = FolderFileLab.get(context);

                                            // Catches null pointer exception that happens on project directory.


                                            // Starts explicit intent for retrieving photo from gallery
                                            // or dropbox.


                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                                try {
                                                    Intent intentGetPhoto = new Intent();
                                                    intentGetPhoto.setType("image/*");
                                                    intentGetPhoto.putExtra(Intent
                                                            .EXTRA_ALLOW_MULTIPLE, true);

                                                    intentGetPhoto.setAction(Intent.ACTION_GET_CONTENT);

                                                    if (intentGetPhoto.getAction() != null) {

                                                        startActivityForResult(Intent.createChooser(intentGetPhoto,
                                                                "Select Picture"), SELECT_PICTURE);
                                                    }
                                                } catch (Exception e) {
                                                    Toast.makeText(context, "Not able to load multiple image" +
                                                            "selector", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Intent intentGetPhoto = new Intent();
                                                intentGetPhoto.setType("image/*");
                                                intentGetPhoto.setAction(Intent.ACTION_GET_CONTENT);
                                                startActivityForResult(Intent.createChooser(intentGetPhoto,
                                                        "Select Picture"), SELECT_PICTURE);
                                            }
                                            break;
                                        }

                                        // Upload Video
                                    case 3:
                                        if (FolderFileLab.get(context).getParentCurrent() == -1) {
                                            Toast.makeText(context, "Can't add video to project directory",
                                                    Toast.LENGTH_SHORT).show();
                                            break;
                                        } else {
                                            lastFragment = "files";
                                            FolderFileLab labVideo = FolderFileLab.get(context);

                                            // Catches null pointer exception that happens on project directory.
                                            if (labVideo.getParentCurrent() == null) {
                                                Toast.makeText(context, "Can't upload files " +
                                                                "to project directory.",
                                                        Toast.LENGTH_SHORT).show();

                                                // Starts explicit intent for retrieving photo from gallery
                                                // or dropbox.
                                            } else {
                                                Intent intentGetVideo = new Intent();
                                                intentGetVideo.setType("video/*");
                                                intentGetVideo.setAction(Intent.ACTION_GET_CONTENT);
                                                startActivityForResult(Intent.createChooser(intentGetVideo,
                                                                "Select Video"),
                                                        SELECT_VIDEO);
                                            }

                                            break;
                                        }
                                        // Take Photo
                                    case 4:

                                        if (FolderFileLab.get(context).getParentCurrent() == -1) {
                                            Toast.makeText(context,
                                                    "Can't add photo to project directory",
                                                    Toast.LENGTH_SHORT).show();
                                            break;
                                        } else {
                                            FolderFileLab lab = FolderFileLab.get(context);
                                            lastFragment = "files";
                                            FolderFile folderFile = new FolderFile();
                                            Integer parentId = FolderFileLab
                                                    .get(context).getParentCurrent();
                                            folderFile.setName("New File");


                                            folderFile.setParent(parentId);
                                            FolderFile parent = lab.getFolderFile(parentId);
                                            parent.addChild(folderFile.getId());
                                            lab.updateFolderFile(parent);

                                            folderFile.setIsPhoto(true);
                                            lab.setRename(folderFile.getId());
                                            lab.addFolderFile(folderFile);

                                            File photoFile = FolderFileLab
                                                    .get(context).getPhotoFile(folderFile);
                                            Intent captureImage =
                                                    new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                                            Uri uri = Uri.fromFile(photoFile);
                                            folderFile.setFilePath(uri.toString());

                                            lab.updateFolderFile(folderFile);
                                            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                                            captureImage.putExtra(PHOTOT_FILE_ID,
                                                    folderFile.getId());
                                            startActivityForResult(captureImage,
                                                    FILE_REQUEST_IMAGE_CAPTURE);
                                            break;
                                        }

                                        // Take Video
                                    case 5:
                                        if (FolderFileLab.get(context).getParentCurrent() == -1) {
                                            Toast.makeText(context, "Can't add video to project directory",
                                                    Toast.LENGTH_SHORT).show();
                                            break;
                                        } else {

                                            lastFragment = "files";
                                            //Create new FolderFile
                                            FolderFileLab lab = FolderFileLab.get(context);
                                            FolderFile videoFile = new FolderFile();

                                            videoFile.setName("New Video");

                                            videoFile.setParent(lab.getParentCurrent());

                                            FolderFile parent = lab.getFolderFile(lab
                                                    .getParentCurrent());
                                            parent.addChild(videoFile.getId());
                                            lab.updateFolderFile(parent);


                                            videoFile.setIsVideo(true);
                                            lab.setRename(videoFile.getId());
                                            lab.addFolderFile(videoFile);

                                            //Set name for file path
                                            File file = FolderFileLab.get(context)
                                                    .getVideoFile(videoFile);
                                            Uri uriFile = Uri.fromFile(file);
                                            videoFile.setFilePath(uriFile.toString());
                                            lab.updateFolderFile(videoFile);

                                            Intent takeVideoIntent = new
                                                    Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                            takeVideoIntent.putExtra
                                                    (MediaStore.EXTRA_OUTPUT, uriFile);
                                            if (takeVideoIntent.resolveActivity(
                                                    context.getPackageManager()) != null) {
                                                startActivityForResult(takeVideoIntent,
                                                        FILE_REQUEST_VIDEO_CAPTURE);
                                            }
                                            break;
                                        }
                                    default:
                                        break;

                                }
                            }
                        }
                ).show();
            }
        });

//d

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            // Used for retrieving pictures from the gallery.
            if (requestCode == SELECT_PICTURE) {
                ClipData clipData = data.getClipData();

                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        try {
                            lastFragment = "files";
                            FolderFileLab lab = FolderFileLab.get(context);

                            FolderFile folderFile = new FolderFile();
                            folderFile.setName("New Photo" + (i + 1));
                            folderFile.setFilePath(clipData.getItemAt(i).getUri().toString());
                            Integer parentId = lab.getParentCurrent();

                            folderFile.setParent(parentId);
                            FolderFile parent = lab.getFolderFile(parentId);
                            parent.addChild(folderFile.getId());
                            lab.updateFolderFile(parent);

                            folderFile.setIsPhoto(true);
                            lab.addFolderFile(folderFile);

                        } catch (Exception e) {
                            Toast.makeText(context, "Error uploading photos"
                                    , Toast.LENGTH_SHORT).show();
                        }
                    }
                    FilesListFragment fragment =
                            (FilesListFragment) getSupportFragmentManager().findFragmentById(
                                    filesFragment.getId());
                    fragment.updateUI();


                } else {

                    lastFragment = "files";
                    Uri selectedImageUri = data.getData();

                    FolderFileLab lab = FolderFileLab.get(this);


                    final FolderFile folderFile = new FolderFile();
                    folderFile.setName("New Image");


                    Integer parentId = lab.getParentCurrent();

                    folderFile.setParent(parentId);
                    FolderFile parent = lab.getFolderFile(parentId);
                    parent.addChild(folderFile.getId());
                    lab.updateFolderFile(parent);

                    folderFile.setFilePath(selectedImageUri.toString());
                    folderFile.setIsPhoto(true);
                    lab.addFolderFile(folderFile);

                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("What do you want to name this file?");
                    final EditText inputPhotoName = new EditText(this);
                    inputPhotoName.setText("New Image");
                    builder.setView(inputPhotoName);
                    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (inputPhotoName.getText() == null || inputPhotoName.length() < 1) {
                                folderFile.setName("New Image");
                            } else {
                                folderFile.setName(inputPhotoName.getText().toString());
                            }
                            FolderFileLab.get(getApplicationContext()).updateFolderFile(folderFile);
                            FilesListFragment fragment =
                                    (FilesListFragment) getSupportFragmentManager().findFragmentById(
                                            filesFragment.getId());
                            fragment.updateUI();
                        }
                    })
                            .setNegativeButton("Cancel", null)
                            .show();
                }


            } else if (requestCode == SELECT_VIDEO) {
                lastFragment = "files";

                Uri selectedVideoUri = data.getData();
                FolderFileLab lab = FolderFileLab.get(this);

                final FolderFile folderFile = new FolderFile();
                folderFile.setName("New Video");
                Integer parentId = lab.getParentCurrent();

                folderFile.setParent(parentId);
                FolderFile parent = lab.getFolderFile(parentId);
                parent.addChild(folderFile.getId());
                lab.updateFolderFile(parent);

                folderFile.setFilePath(selectedVideoUri.toString());
                folderFile.setIsVideo(true);
                lab.addFolderFile(folderFile);

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("What do you want to name this file?");
                final EditText inputPhotoName = new EditText(this);
                inputPhotoName.setText("New Video");
                builder.setView(inputPhotoName);
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (inputPhotoName.getText() == null || inputPhotoName.length() < 1) {
                            folderFile.setName("New Video");
                        } else {
                            folderFile.setName(inputPhotoName.getText().toString());
                        }
                        FolderFileLab.get(getApplicationContext()).updateFolderFile(folderFile);
                        FilesListFragment fragment =
                                (FilesListFragment) getSupportFragmentManager().findFragmentById(
                                        filesFragment.getId());
                        fragment.updateUI();
                    }
                })
                        .setNegativeButton("Cancel", null)
                        .show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Changes the view to what tab should be displayed

        if (lastFragment == null || lastFragment.equals("files")) {
            getSupportActionBar().setSelectedNavigationItem(0);

        }else if (lastFragment.equals("contacts")) {
            getSupportActionBar().setSelectedNavigationItem(1);
        } else {
            // Opens up FilesListFragment so that it can rename new pictures onResume.
            getSupportActionBar().setSelectedNavigationItem(0);
        }
    }


    /**
     * Custom Implementation of OnItemClickListener to handle clicks in the app drawer.
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }
}
