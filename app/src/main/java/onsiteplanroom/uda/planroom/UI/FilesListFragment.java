package onsiteplanroom.uda.planroom.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import onsiteplanroom.uda.planroom.Details.ImageViewPager;
import onsiteplanroom.uda.planroom.Details.ProjectDetailActivity;
import onsiteplanroom.uda.planroom.Dialogs.FileChooser;
import onsiteplanroom.uda.planroom.Dialogs.MultipleFileChooser;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.Utilities.PictureUtils;
import onsiteplanroom.uda.planroom.WebServices.RefreshFiles;
import onsiteplanroom.uda.planroom.labs.ContactLab;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 2/2/2016.
 * This class is hosted by the main activity and defines one of the 3 main fragments in the app.
 * This class implements a mFile system. The way the mFile is system is kind of like a Linked list.
 * Each FolderFile has a pointer towards its parent and contains an arraylist of all of its children.
 * If the FolderFile is a mFile then the detail view opens when pressed. If the FolderFile is a
 * project then the project detail is opened when the info button is pressed. If the list item is
 * pressed it loads its children. If the FolderFile is a mFile then on press its children are loaded.
 * The current parent is always saved in the FolderFileLab so it can be accessed anywhere anytime.
 * Once you press the up button the adapter loads the Current parent's parent's children, which
 * represents the up mFile.
 * <p/>
 * This class contains some complicated logic and has to take into account the different types
 * of FolderFiles that exist.
 * <p/>
 * As a general rule of thumb when adding a file/folder/project you need to designate the parent
 * as well as add it to the parent's children ArrayList. Remember to update parent after you
 * add it.
 */

public class FilesListFragment extends Fragment implements RecyclerView.OnItemTouchListener,
        ActionMode.Callback {

    // ******
    // FIELDS
    // ******

    private FilesListFragment selfFrag;
    private RecyclerView mFilesRecyclerView;
    private FileAdapter mAdapter;
    private String currentFolderChangeName;
    private ActionMode actionMode;
    private static final String MULTIPLE_FILE_MOVE = "multiple_files";
    private SwipeRefreshLayout mRefreshButton;


    // ****************
    // OVERRIDE METHODS
    // ****************

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FolderFileLab lab = FolderFileLab.get(getContext());
        Integer updateId = (Integer) getActivity()
                .getIntent().getSerializableExtra("update_folder_id");

        // Thread stuff to download thumbnail.

        List<FolderFile> tempList = new ArrayList<>();
        for (FolderFile file : lab.getFolderFiles()) {
            if (file.isPhoto()) {
                tempList.add(file);
            }
        }

        selfFrag = this;

        // This is used to save the place of the user if they switched to a different tab for a
        // second and wanted to return back to where they were in the mFile tree.
        if (updateId != null) {
            lab.setParentCurrent(updateId);
            getActivity().getIntent().removeExtra("update_folder_id");
        }

        // If the parent has not been set yet (on start up) then set the current parent to null
        // (the project directory/the highest directory)
        if (lab.getParentCurrent() == null) {
            lab.setParentCurrent(-1);
        }


        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.file_list_menu, menu);
    }


    // This class has several menu items:
    // Go up a folder.
    // Take a photo.
    // Add a folder.

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            // Logout
            case R.id.file_logout:
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();

                SharedPreferences prefs = getActivity().getSharedPreferences("user_pref",
                        Context.MODE_PRIVATE);
                prefs.edit().remove("user_pref").apply();
                FolderFileLab.get(getContext()).dropFolderFileTable();
                ContactLab.get(getContext()).dropContactTable();
                return true;

            // Go up a folder.
            case R.id.menu_list_go_up:

                Integer newParent;
                FolderFileLab lab = FolderFileLab.get(getContext());
                lab.clearMapFromMemory();
                if (lab.getFolderFile(lab.getParentCurrent()) == null) {
                    newParent = null;
                } else {
                    newParent = lab.getFolderFile(
                            lab.getParentCurrent()).getParent();
                }
                if (newParent == null || lab.getFolderFile(lab.getParentCurrent()).isProject()) {
                    lab.setParentCurrent(-1);
                    updateUI();
                } else {
                    lab.setParentCurrent(newParent);
                    updateUI();
                }

                return true;
            // Add a folder.
            case R.id.add_folder_button:

                if (FolderFileLab.get(getContext()).getParentCurrent() == -1) {
                    Toast.makeText(getContext(), "Can't add folder to project directory",
                            Toast.LENGTH_SHORT).show();
                    return false;
                } else {


                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Folder Name");
                    alertDialog.setMessage("What do you want the folder name to be?");

                    final EditText input = new EditText(getActivity());
                    input.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(
                                CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(
                                CharSequence s, int start, int before, int count) {
                            currentFolderChangeName = s.toString();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    alertDialog.setView(input);
                    alertDialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FolderFileLab lab = FolderFileLab.get(getContext());
                            int parentId = lab.getParentCurrent();
                            FolderFile file = new FolderFile();

                            if (currentFolderChangeName == null || currentFolderChangeName
                                    .length() < 1) {
                                file.setName("New Folder");
                            } else {
                                file.setName(input.getText().toString());

                            }
                            file.setParent(parentId);
                            FolderFile parentFile = lab.getFolderFile(parentId);
                            parentFile.addChild(file.getId());
                            lab.updateFolderFile(parentFile);
                            lab.addFolderFile(file);
                            currentFolderChangeName = null;
                            updateUI();

                        }
                    });
                    alertDialog.setNegativeButton("cancel", null);
                    alertDialog.show();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        FolderFileLab lab = FolderFileLab.get(getContext());
        lab.clearMapFromMemory();

    }

    @Override
    public void onResume() {
        super.onResume();


        // On resume the fragment checks to see if there is a mFile that needs to be renamed
        // (recently added mFile) and if there is it launches a dialog that asks for the name.

        FolderFileLab lab = FolderFileLab.get(getContext());

        if (lab.getParentCurrent() == -1) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Projects");
        } else {
            String tempName = lab.getFolderFile(lab.getParentCurrent()).getName();
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tempName);
        }


        if (lab.shouldRename()) {

            Integer tempId = lab.getRename();


            final FolderFile folderFile = lab.getFolderFile(tempId);

            Bitmap bitmap;
            if (folderFile.isPhoto()) {
                File file = FolderFileLab.get(getContext()).getPhotoFile(folderFile);
                bitmap = PictureUtils.getScaledBitmap(file.getPath(), getActivity());
            } else {
                FolderFileLab labVid = FolderFileLab.get(getContext());

                File fileOfVid = labVid.getVideoFile(folderFile);
                bitmap = ThumbnailUtils.createVideoThumbnail(fileOfVid.getAbsolutePath()
                        , MediaStore.Video.Thumbnails.MICRO_KIND);
            }

            if (bitmap != null) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("What do you want to name this file?");
                final EditText inputPhotoName = new EditText(getActivity());
                inputPhotoName.setText("New Capture");
                builder.setView(inputPhotoName);
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (inputPhotoName.getText() == null ||
                                inputPhotoName.getText().length() < 1) {
                            folderFile.setName("New Capture");
                        } else {
                            folderFile.setName(inputPhotoName.getText().toString());
                        }
                        FolderFileLab.get(getContext()).updateFolderFile(folderFile);

                        //Resets the rename because it is a static variable.
                        FolderFileLab.get(getContext()).setRename(-2);
                        updateUI();
                    }
                })
                        .setNegativeButton("Cancel", null)
                        .show();
            } else {
                FolderFileLab.get(getContext()).deleteFolderFile(folderFile);
            }
        }
        updateUI();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.files_container, container, false);
        mFilesRecyclerView = (RecyclerView) view.findViewById(R.id.files_recycler_view);
        mFilesRecyclerView.setLayoutManager(new
                LinearLayoutManager(getActivity()));
        mFilesRecyclerView.addOnItemTouchListener(this);
        mRefreshButton = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mRefreshButton.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });

        updateUI();
        return view;
    }

    private void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                // Shared Preferences to get user and password then load up contact in database
                SharedPreferences prefs = getActivity().getSharedPreferences(
                        "user_pref", Context.MODE_PRIVATE);
                String user_pref = prefs.getString("user_pref", null);
                String password_pref = prefs.getString("password_pref", null);
                String date_pref = prefs.getString("files_last_refreshed", null);

                RefreshFiles.refreshFolderFiles(getContext(), user_pref, password_pref,
                        date_pref, mRefreshButton, selfFrag);

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");
                String nowDate = df.format(new Date());

                prefs.edit().putString("files_last_refreshed", nowDate).apply();
            }
        }, 0);
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        this.actionMode = null;
        mAdapter.clearSelections();
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

        //This is the menu switch structure for the multiselect mode.


        switch (menuItem.getItemId()) {

            // Delete multiple files.
            case R.id.selection_file_delete_icon:


                ProgressDialog progressDialog = new ProgressDialog(getContext());

                progressDialog.setMessage("Deleting Files");


                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                if (mAdapter.getSelectedItemCount() == 1) {
                    builder.setMessage("Are you sure you want to delete this item");
                } else {
                    builder.setMessage("Are you sure you want to delete these "
                            + mAdapter.getSelectedItemCount() + " items");
                }
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        FolderFileLab lab = FolderFileLab.get(getContext());


                        List<Integer> selectedItemPositions =
                                mAdapter.getSelectedItems();
                        FolderFile parent = lab.getFolderFile(lab.getParentCurrent());
                        List<Integer> children = parent.getChildren();
                        List<FolderFile> childrenFiles = new ArrayList<>();


                        for (Integer child : children) {
                            childrenFiles.add(lab.getFolderFile(child));
                        }

                        for (int i = 0; i < selectedItemPositions.size(); i++) {
                            FolderFile childToRemove =
                                    childrenFiles.get(selectedItemPositions.get(i));
                            lab.deleteFolderWithChildren(childToRemove.getId());
                            lab.deleteFolderFile(childToRemove);
                            parent.removeChild(childToRemove.getId());
                        }

                        lab.updateFolderFile(parent);
                        mAdapter.clearSelections();
                    }
                });
                builder.setNegativeButton("No", null);
                builder.show();


                return true;

            //Move multiple files.
            case R.id.selection_file_move_icon:


                FolderFileLab lab2 = FolderFileLab.get(getContext());
                Integer currentParentId = lab2.getParentCurrent();
                List<FolderFile> files2 = new ArrayList<>();
                List<FolderFile> selectedFiles = new ArrayList<>();
                List<Integer> children2 = new ArrayList<>();
                List<Integer> selectedItemPositions2 =
                        mAdapter.getSelectedItems();
                ArrayList<String> stringIds = new ArrayList<>();

                if (currentParentId == -1) {
                    for (FolderFile file : lab2.getFolderFiles()) {
                        if (file.isProject()) {
                            files2.add(file);
                            children2.add(file.getId());
                        }
                    }
                } else {
                    FolderFile parent2 = lab2.getFolderFile(lab2.getParentCurrent());
                    children2 = parent2.getChildren();


                    for (Integer id : children2) {
                        files2.add(lab2.getFolderFile(id));
                    }
                }

                Collections.sort(files2);

                for (int i = 0; i < selectedItemPositions2.size(); i++) {
                    selectedFiles.add(files2.get(selectedItemPositions2.get(i)));
                }
                for (FolderFile file : selectedFiles) {
                    stringIds.add(file.getId().toString());
                }

                Bundle bundle = new Bundle();
                bundle.putStringArrayList(MULTIPLE_FILE_MOVE, stringIds);
                FragmentManager fm = getActivity().
                        getSupportFragmentManager();
                MultipleFileChooser multipleFileChooser = new MultipleFileChooser();
                multipleFileChooser.setArguments(bundle);
                multipleFileChooser.parentView = selfFrag;
                multipleFileChooser.show(fm, "Multiple Chooser Fragment");
                mAdapter.clearSelections();

                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.folder_file_select, menu);
        return true;
    }

    // *******
    // METHODS
    // *******

    private void myToggleSelection(int idx) {
        mAdapter.toggleSelection(idx);
        String title = getString(R.string.selected_count, mAdapter.getSelectedItemCount());
        actionMode.setTitle(title);

    }

    /**
     * Updates the view. This is used quite a bit to reload the mFile tree and to update items when
     * their names have been changed or an action has been performed on them.
     */

    public void updateUI() {
        FolderFileLab lab = FolderFileLab.get(getContext());


        List<FolderFile> files = lab.getFolderFiles();
        Collections.sort(files);
        if (lab.getParentCurrent() == -1) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Files");
        } else {
            String tempName = lab.getFolderFile(lab.getParentCurrent()).getName();
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(tempName);
        }
        mAdapter = null;
        mAdapter = new FileAdapter(files);
        mFilesRecyclerView.setAdapter(mAdapter);

    }


    // *************
    // Inner Classes
    // *************

    /**
     * This is the Holder for the Files List Fragment. This sets up the on click listeners and the
     * on long click listeners for each individual list item.
     */

    private class FileHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {

        // ******
        // FIELDS
        // ******

        public FolderFile mFolderFile;
        private TextView mSubjectTextView;
        private TextView mDateModifiedTextView;
        public ImageView mVideoOverlay;
        public ImageView mImagePreview;
        private ImageView mInfoButton;
        private String renameTextString;
        private static final String ID_EXTRA_PROJECT = "id_project";
        private static final String ID_EXTRA_TO_CHOOSER = "id_to_chooser";
        public RelativeLayout itemBackground;

        // ***********
        // Constructor
        // ***********

        public FileHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            mSubjectTextView = (TextView) itemView
                    .findViewById(R.id.list_item_file_subject_textview);
            mDateModifiedTextView = (TextView) itemView
                    .findViewById(R.id.list_item_file_date_modified);
            mImagePreview = (ImageView) itemView
                    .findViewById(R.id.file_preview_image);
            mInfoButton = (ImageView) itemView
                    .findViewById(R.id.info_button_project_list_item);
            mVideoOverlay = (ImageView) itemView
                    .findViewById(R.id.play_button_overlay_files);
            itemBackground = (RelativeLayout) itemView
                    .findViewById(R.id.folderfile_list_item_layout);
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public void onClick(View v) {
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.lastFragment = "files";
            if (actionMode != null) {


                if (!mFolderFile.isProject()) {


                    if (mAdapter.selectedItems.get(getPosition())) {
                        itemBackground.setBackground(null);
                    } else {
                        itemBackground.setBackgroundColor(Color.parseColor("#B2EBF2"));
                    }

                    int idx = mFilesRecyclerView.getChildPosition(v);
                    myToggleSelection(idx);

                } else {
                    Toast.makeText(getContext(),
                            "Can't move project from project directory", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (mFolderFile.isPhoto() || mFolderFile.isVideo()) {
                    // Launches the gallery
                    Intent intent = ImageViewPager.newIntent(getActivity(), mFolderFile.getId());
                    startActivity(intent);
                    // Folders
                } else {

                    FolderFileLab lab = FolderFileLab.get(getContext());

                    lab.setParentCurrent(mFolderFile.getId());
                    updateUI();
                }
            }
        }

        @Override
        public boolean onLongClick(View v) {

            // long clicking launches multiselect mode.

            if (!mFolderFile.isProject()) {
                if (actionMode == null) {
                    actionMode = getActivity().startActionMode(FilesListFragment.this);
                }

                if (mAdapter.selectedItems.get(getPosition())) {
                    itemBackground.setBackground(null);
                } else {
                    itemBackground.setBackgroundColor(Color.parseColor("#B2EBF2"));
                }


                int idx = mFilesRecyclerView.getChildPosition(v);

                myToggleSelection(idx);

                return true;
            } else {
                Toast.makeText(getContext(),
                        "Can't move project from project directory", Toast.LENGTH_SHORT).show();
                return true;
            }
        }

        // *******
        // METHODS
        // *******


        /**
         * This method binds each individual mFile to the adapter. This method also provides
         * the on click listeners for the info button on the right as well as defining the dialogs.
         *
         * @param file - the mFile that will be binded.
         */

        public void bindFile(FolderFile file) {
            mFolderFile = file;
            mSubjectTextView.setText(mFolderFile.getName());

            mImagePreview.setTag(mFolderFile.getPhotoFileName());

            mVideoOverlay.setVisibility(View.INVISIBLE);


            Date date = mFolderFile.getModifiedDate();
            Date today = new Date();
            if ((date.getYear() == today.getYear()) && (date.getMonth() == today.getMonth()) &&
                    (date.getDay() == today.getDay())) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("KK:mm a");
                String dateString = simpleDateFormat.format(date);
                mDateModifiedTextView.setText("Last Modified " + dateString);

            } else {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy KK:mm a");
                String dateString = simpleDateFormat.format(date);
                mDateModifiedTextView.setText("Last Modified " + dateString);
            }

            if (!mFolderFile.isProject()) {

                mInfoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String itemType;

                        if (mFolderFile.isPhoto() || mFolderFile.isVideo()) {
                            itemType = "File";
                        } else {
                            itemType = "Folder";
                        }
                        final CharSequence[] items = {
                                "Rename " + itemType, "Delete " + itemType, "Move " + itemType
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                        if (mFolderFile.isPhoto() || mFolderFile.isVideo()) {
                            builder.setTitle("File Actions");
                        } else {
                            builder.setTitle("Folder Actions");
                        }
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {

                                    // Rename
                                    case 0:
                                        AlertDialog.Builder alertDialog = new
                                                AlertDialog.Builder(getActivity());

                                        if (mFolderFile.isPhoto() || mFolderFile.isVideo()) {
                                            alertDialog.setTitle("Rename File");
                                        } else {
                                            alertDialog.setTitle("Rename Folder");
                                        }

                                        alertDialog.setMessage(
                                                "What do you want the new name to be?");

                                        final EditText input = new EditText(getActivity());
                                        input.setText(mFolderFile.getName());
                                        input.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(
                                                    CharSequence s, int start, int count,
                                                    int after) {

                                            }

                                            @Override
                                            public void onTextChanged(
                                                    CharSequence s, int start, int before,
                                                    int count) {
                                                renameTextString = s.toString();
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {

                                            }
                                        });
                                        alertDialog.setView(input);
                                        alertDialog.setPositiveButton("Save",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {
                                                        if (renameTextString == null
                                                                || renameTextString
                                                                .length() < 1) {

                                                            if (mFolderFile.isPhoto() ||
                                                                    mFolderFile.isVideo()) {
                                                                mFolderFile.setName("New File");
                                                            } else {
                                                                mFolderFile.setName("New Folder");
                                                            }
                                                            mFolderFile.setModifiedDate(new Date());
                                                            FolderFileLab.get(getContext())
                                                                    .updateFolderFile(mFolderFile);
                                                            renameTextString = null;
                                                            updateUI();
                                                        } else {
                                                            mFolderFile.setName(renameTextString);
                                                            mFolderFile.setModifiedDate(new Date());
                                                            FolderFileLab.get(getContext())
                                                                    .updateFolderFile(mFolderFile);
                                                            renameTextString = null;
                                                            updateUI();
                                                        }
                                                    }
                                                });
                                        alertDialog.setNegativeButton("cancel", null);
                                        alertDialog.show();
                                        break;

                                    // Delete
                                    case 1:
                                        AlertDialog.Builder deleteWarning = new AlertDialog.Builder(
                                                getContext());
                                        deleteWarning.setMessage("Are you sure you want" +
                                                " to delete this?");
                                        deleteWarning.setPositiveButton("Yes", new DialogInterface
                                                .OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                FolderFileLab lab = FolderFileLab.get(getContext());


                                                Integer id = lab.getParentCurrent();
                                                if (id != -1) {
                                                    FolderFile parent = lab.getFolderFile(id);
                                                    parent.removeChild(mFolderFile.getId());
                                                    lab.updateFolderFile(parent);
                                                }
                                                lab.deleteFolderWithChildren(mFolderFile.getId());
                                                lab.deleteFolderFile(mFolderFile);

                                                updateUI();
                                            }
                                        })
                                                .setNegativeButton("No", null).show();
                                        break;

                                    // Move
                                    case 2:
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(ID_EXTRA_TO_CHOOSER,
                                                mFolderFile.getId());
                                        FragmentManager fm = getActivity().
                                                getSupportFragmentManager();
                                        FileChooser fileChooserDialog = new FileChooser();
                                        fileChooserDialog.setArguments(bundle);
                                        fileChooserDialog.parentView = selfFrag;
                                        fileChooserDialog.show(fm, "Chooser Fragment");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        });
                        builder.show();
                    }
                });
            } else {
                mInfoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), ProjectDetailActivity.class);
                        intent.putExtra(ID_EXTRA_PROJECT, mFolderFile.getId().toString());
                        startActivity(intent);

                    }
                });
            }
        }
    }

    /**
     * This class is the adapter for the Files List Fragment items. It configures how I want my
     * FolderFile items to be displayed in the list.
     */

    private class FileAdapter extends RecyclerView.Adapter<FileHolder> {

        // ******
        // FIELDS
        // ******

        private List<FolderFile> mFiles;
        private List<FolderFile> tempFiles = new ArrayList<>();
        public SparseBooleanArray selectedItems;

        // ***********
        // Constructor
        // ***********

        public FileAdapter(List<FolderFile> files) {
            FolderFileLab lab = FolderFileLab.get(getContext());
            selectedItems = new SparseBooleanArray();

            mFiles = files;
            if (lab.getParentCurrent() == -1) {
                for (FolderFile file : mFiles) {
                    if (file.isProject()) {
                        tempFiles.add(file);
                    }
                }
            } else {
                if (lab.getFolderFile(lab.getParentCurrent())
                        .getChildren() == null || lab.getFolderFile(lab.getParentCurrent())
                        .getChildren().size() < 1) {
                    mFiles = null;
                } else {
                    List childrenOfClicked =
                            lab.getFolderFile(lab.getParentCurrent()).getChildren();

                    for (int i = 0; i < childrenOfClicked.size(); i++) {
                        tempFiles.add(lab.getFolderFile((int) childrenOfClicked.get(i)));
                    }
                }
            }
            mFiles = tempFiles;
        }

        // *******
        // METHODS
        // *******

        public void toggleSelection(int pos) {
            if (selectedItems.get(pos, false)) {
                selectedItems.delete(pos);
            } else {
                selectedItems.put(pos, true);
            }
        }

        public void clearSelections() {
            if (selectedItems != null) {

                selectedItems.clear();
                updateUI();
            }

        }

        public int getSelectedItemCount() {
            return selectedItems.size();
        }

        public List<Integer> getSelectedItems() {
            List<Integer> items =
                    new ArrayList<Integer>(selectedItems.size());

            for (int i = 0; i < selectedItems.size(); i++) {
                items.add(selectedItems.keyAt(i));
            }
            return items;
        }

        // ****************
        // OVERRIDE METHODS
        // ****************

        @Override
        public FileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_folderfile, parent, false);
            ImageView preview = (ImageView) view.findViewById(R.id.file_preview_image);


            switch (viewType) {
                case 0:
                    Drawable drawable = getResources().getDrawable(R.mipmap.project_blu);
                    preview.setBackground(drawable);
                    break;
                case 1:
                    Drawable drawableFile = getResources().getDrawable(R.mipmap.picture_blu);
                    preview.setBackground(drawableFile);
                    break;
                case 3:
                    Drawable drawableVid = getResources().getDrawable(R.mipmap.videocam);
                    preview.setBackground(drawableVid);
                    break;
                default:
                    //
            }

            return new FileHolder(view);
        }

        @Override
        public int getItemViewType(int position) {
            FolderFile file = mFiles.get(position);

            if (file != null) {

                //Project
                if (file.isProject()) {
                    return 0;

                    // Photo
                } else if (file.isPhoto()) {
                    return 1;

                    //Video
                } else if (file.isVideo()) {
                    return 3;

                    //Folder
                } else {
                    return 2;
                }
            }
            return 4;
        }

        @Override
        public void onBindViewHolder(FileHolder holder, int position) {
            FolderFile file = mFiles.get(position);
            FolderFileLab lab = FolderFileLab.get(getContext());
            holder.bindFile(file);

            if (actionMode != null) {

                if (!selectedItems.get(position)) {
                    holder.itemBackground.setBackground(null);
                } else {
                    holder.itemBackground.setBackgroundColor(Color.parseColor("#B2EBF2"));
                }
            }

            if (file.isPhoto() || file.isVideo()) {

                Drawable placeholder;
                if (file.isPhoto()) {
                    placeholder = getResources().getDrawable(R.mipmap.picture_blu);
                } else {
                    placeholder = getResources().getDrawable(R.mipmap.videocam);
                }
                holder.mImagePreview.setBackground(placeholder);
                holder.mImagePreview.setImageBitmap(null);

                // Picasso loads the thumbnail for me.
                Picasso
                        .with(getContext())
                        .load("http://files.constructiononline.com/picture/TINY/"
                                + file.getId() + "/")
                        .fit()
                        .centerCrop()
                        .into(holder.mImagePreview);

            }

        }

        @Override
        public int getItemCount() {
            return mFiles.size();
        }

    }
}