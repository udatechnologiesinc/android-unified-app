package onsiteplanroom.uda.planroom.Objects;

import java.util.Date;
import java.util.Random;

/**
 * Created by Harry on 2/2/2016.
 * This class defines the Contact object.
 */
public class Contact implements Comparable<Contact> {

    private Integer mId;
    private String mFirstName;
    private String mLastName;
    private String mRole;
    private Date mDateModified;
    private String mEmail;
    private String mAccount;
    // Last Accessed
    // Last Modified
    private String mDisplayName;
    private String mCompany;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mZip;
    private String mPhone;
    private String mMobilePhone;
    private String mMobileProvider;
    private String mFax;
    private Integer mLastModifiedContactId;
    private String mLastModifiedname;
    private boolean mIsCompany;
    private String mMiddleName;
    private String mWebsite;
    private String mSummary;
    private String mExperience;
    private Integer mCreatorId;
    private String mCreatorName;
    private double mTimezoneOffset;
    private boolean mAutoTimezone;
    private Integer mCompanyId;
    private String mCompanyName;
    private Integer mCountrySetting;
    private Integer mLanguageSetting;
    private double mLat;
    private double mLon;
    // CS LMOD
    private Integer mCompanyPermissions;
    private boolean mIsCompanyAdmin;
    private String mPremium;
    private String mMobileTokens;
    private String mSubscription;
    private Integer mPremiumType;
    private String mField;


    //**************************DELETE THIS EVENTUALLY

    public Contact() {
        this(new Random().nextInt(100000));
    }

    //**************************DELETE THIS EVENTUALLY

    public Contact(Integer id) {
        mId = id;

        if (mDateModified == null) {
            mDateModified = new Date();
        }
    }

    public int compareTo(Contact contact) {

        String thisName = this.getFirstName() +  this.getLastName();
        String thatName = contact.getFirstName() + contact.getLastName();
        thisName = thisName.toUpperCase();
        thatName = thatName.toUpperCase();

        return thisName.compareTo(thatName);
    }


    public Integer getId() {
        return mId;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public Date getDateModified() {
        return mDateModified;
    }

    public void setDateModified(Date dateModified) {
        mDateModified = dateModified;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getAccount() {
        return mAccount;
    }

    public void setAccount(String account) {
        mAccount = account;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        mZip = zip;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getMobilePhone() {
        return mMobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        mMobilePhone = mobilePhone;
    }

    public String getMobileProvider() {
        return mMobileProvider;
    }

    public void setMobileProvider(String mobileProvider) {
        mMobileProvider = mobileProvider;
    }

    public String getFax() {
        return mFax;
    }

    public void setFax(String fax) {
        mFax = fax;
    }

    public Integer getLastModifiedContactId() {
        return mLastModifiedContactId;
    }

    public void setLastModifiedContactId(Integer lastModifiedContactId) {
        mLastModifiedContactId = lastModifiedContactId;
    }

    public String getLastModifiedname() {
        return mLastModifiedname;
    }

    public void setLastModifiedname(String lastModifiedname) {
        mLastModifiedname = lastModifiedname;
    }

    public boolean isCompany() {
        return mIsCompany;
    }

    public void setIsCompany(boolean isCompany) {
        mIsCompany = isCompany;
    }

    public String getMiddleName() {
        return mMiddleName;
    }

    public void setMiddleName(String middleName) {
        mMiddleName = middleName;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String website) {
        mWebsite = website;
    }

    public String getSummary() {
        return mSummary;
    }

    public void setSummary(String summary) {
        mSummary = summary;
    }

    public String getExperience() {
        return mExperience;
    }

    public void setExperience(String experience) {
        mExperience = experience;
    }

    public Integer getCreatorId() {
        return mCreatorId;
    }

    public void setCreatorId(Integer creatorId) {
        mCreatorId = creatorId;
    }

    public String getCreatorName() {
        return mCreatorName;
    }

    public void setCreatorName(String creatorName) {
        mCreatorName = creatorName;
    }

    public double getTimezoneOffset() {
        return mTimezoneOffset;
    }

    public void setTimezoneOffset(double timezoneOffset) {
        mTimezoneOffset = timezoneOffset;
    }

    public boolean isAutoTimezone() {
        return mAutoTimezone;
    }

    public void setAutoTimezone(boolean autoTimezone) {
        mAutoTimezone = autoTimezone;
    }

    public Integer getCompanyId() {
        return mCompanyId;
    }

    public void setCompanyId(Integer companyId) {
        mCompanyId = companyId;
    }

    public Integer getCountrySetting() {
        return mCountrySetting;
    }

    public void setCountrySetting(Integer countrySetting) {
        mCountrySetting = countrySetting;
    }

    public Integer getLanguageSetting() {
        return mLanguageSetting;
    }

    public void setLanguageSetting(Integer languageSetting) {
        mLanguageSetting = languageSetting;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLon() {
        return mLon;
    }

    public void setLon(double lon) {
        mLon = lon;
    }

    public Integer getCompanyPermissions() {
        return mCompanyPermissions;
    }

    public void setCompanyPermissions(Integer companyPermissions) {
        mCompanyPermissions = companyPermissions;
    }

    public boolean isCompanyAdmin() {
        return mIsCompanyAdmin;
    }

    public void setIsCompanyAdmin(boolean isCompanyAdmin) {
        mIsCompanyAdmin = isCompanyAdmin;
    }

    public String getPremium() {
        return mPremium;
    }

    public void setPremium(String premium) {
        mPremium = premium;
    }

    public String getMobileTokens() {
        return mMobileTokens;
    }

    public void setMobileTokens(String mobileTokens) {
        mMobileTokens = mobileTokens;
    }

    public String getSubscription() {
        return mSubscription;
    }

    public void setSubscription(String subscription) {
        mSubscription = subscription;
    }

    public Integer getPremiumType() {
        return mPremiumType;
    }

    public void setPremiumType(Integer premiumType) {
        mPremiumType = premiumType;
    }

    public String getField() {
        return mField;
    }

    public void setField(String field) {
        mField = field;
    }
}
