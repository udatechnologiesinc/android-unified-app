package onsiteplanroom.uda.planroom.Objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Harry on 2/2/2016.
 * This class defines the FolderFile object.
 * Each FolderFile contains a universal unique identifier.
 * The class is called a FolderFile because it defines both Folders and Files.
 * It also defines projects at the moment but that may be temporary.
 */
public class FolderFile implements Comparable<FolderFile> {
    private Integer mId;
    private String mName;
    private List<Integer> mChildren;

    // Project Fields
    private Integer mCreatedBy;
    private String mCreatorName;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mZip;
    private String mProjectNumber;
    private Integer mLastModifiedContactId;
    private String mLastModifiedName;
    private String mProjectType;
    private Integer mContactId;
    private String mContactName;
    private boolean mIsArchived;
    private boolean mIsCanadian;
    private Integer mCurrency;
    private boolean mIsTemplate;
    private Integer mCountrySetting;
    private String mCustomColor;
    private Integer mCompanyId;
    private String mCompanyName;
    private boolean mIsInactive;
    private double mLat;
    private double mLon;
    private String mScopeOfWork;
    private boolean mIsUngrouped;
    private Integer mTotalTime;
    private double mTotalMoney;
    private String mProjectNotes;
    private Integer mNumberFolders;
    private Integer mNumberImages;
    private Integer mNumberAlbums;
    private Integer mNumberFiles;
    private Integer mNumberComments;

    // Files Fields
    private String mExt;
    private Integer mSize;
    private String mType;
    private String mParentName;
    private Integer mProjectId;
    private String mProjectName;
    private String mTinyUrl;
    private String mMd5;
    private boolean mIsDeleted;
    private boolean mIsAlbum;
    private Integer mNumberOfChildren;
    private boolean mIsFolder;

    private String mCaption;
    private String mFullName;
    private boolean mIsRecentAttachment;
    private boolean mIsPermAttachment;
    private boolean mIsPrivate;
    private boolean mIsClientFolder;
    private boolean mIsClientUploadFolder;
    private boolean mIsSubConFolder;
    private boolean mIsSharedWithClients;
    private boolean mSharedWithSubs;

    private Date mDateCreated;
    private Date mDateTaken;
    private Date mLastAccessed;

    private Integer mParent;
    private Date mModifiedDate;
    private boolean mIsProject;
    private boolean mIsPhoto;
    private String mFilePath;
    private boolean mIsVideo;
    private boolean mIsOffline;

    public FolderFile(Integer id) {
        mId = id;


        if (mModifiedDate == null) {
            mModifiedDate = new Date();
        }
        if (mDateCreated == null) {
            mDateCreated = new Date();
        }
        if (mDateTaken == null) {
            mDateTaken = new Date();
        }
        if (mLastAccessed == null) {
            mLastAccessed = new Date();
        }
    }

    //***********************************************************
    // WARNING: BAD CODE! POTENTIAL ID OVERLAP THAT WILL WRECK EVERYTHING.
    // WOULD RESULT IN FORCE UNINSTALL OF APP.
    // KEPT THIS TEMPORARILY SO THAT I CAN MAKE APP WORK UNTIL I CAN POST WITH
    // SERVICES THAT WILL REQUEST AN ID FROM COL
    // THIS CODE SHOULD BE DELETED BEFORE ANY SORT OF ROLL OUT!!
   // ************************************ DELETE THIS EVENTUALLY
    public FolderFile() {
        this(new Random().nextInt(10000000) + 10);
    }
    // ************************************ DELETE THIS EVENTUALLY
    //
    // // BAD CONSTRUCTOR ABOVE! CALLED WH
    //
    //********************************************************



    public List<Integer> getChildren() {
        return mChildren;
    }

    public void addChild(int childId) {
        if (mChildren == null) {
            mChildren = new ArrayList<>();
        }

        mChildren.add(childId);
    }

    public void removeChild(Integer childId) {
        mChildren.remove( childId);
    }

    public int compareTo(FolderFile that) {
        return this.getName().compareTo(that.getName());
    }


    public Integer getId() {
        return mId;
    }

    public Integer getParent() {
        return mParent;
    }

    public void setParent(Integer parent) {
        mParent = parent;
    }



    public boolean isVideo() {
        return mIsVideo;
    }

    public void setIsVideo(boolean isVideo) {
        mIsVideo = isVideo;
    }

    public String getPhotoFileName() {
        return "IMG_" + getId().toString() + ".jpg";
    }
    public String getVideoFileName() {
        return "VID_" + getId().toString() + ".mp4";
    }


    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Date getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        mModifiedDate = modifiedDate;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public boolean isProject() {
        return mIsProject;
    }

    public void setIsProject(boolean isProject) {
        mIsProject = isProject;
    }

    public boolean isPhoto() {
        return mIsPhoto;
    }

    public void setIsPhoto(boolean isFile) {
        mIsPhoto = isFile;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Integer getCreatedBy() {
        return mCreatedBy;
    }

    public void setCreatedBy(Integer createdBy) {
        mCreatedBy = createdBy;
    }

    public String getCreatorName() {
        return mCreatorName;
    }

    public void setCreatorName(String creatorName) {
        mCreatorName = creatorName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getZip() {
        return mZip;
    }

    public void setZip(String zip) {
        mZip = zip;
    }

    public String getProjectNumber() {
        return mProjectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        mProjectNumber = projectNumber;
    }

    public Integer getLastModifiedContactId() {
        return mLastModifiedContactId;
    }

    public void setLastModifiedContactId(Integer lastModifiedContactId) {
        mLastModifiedContactId = lastModifiedContactId;
    }

    public String getLastModifiedName() {
        return mLastModifiedName;
    }

    public void setLastModifiedName(String lastModifiedName) {
        mLastModifiedName = lastModifiedName;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setContactName(String contactName) {
        mContactName = contactName;
    }

    public boolean isArchived() {
        return mIsArchived;
    }

    public void setIsArchived(boolean isArchived) {
        mIsArchived = isArchived;
    }

    public boolean isCanadian() {
        return mIsCanadian;
    }

    public void setIsCanadian(boolean isCanadian) {
        mIsCanadian = isCanadian;
    }

    public Integer getCurrency() {
        return mCurrency;
    }

    public void setCurrency(Integer currency) {
        mCurrency = currency;
    }

    public boolean isTemplate() {
        return mIsTemplate;
    }

    public void setIsTemplate(boolean isTemplate) {
        mIsTemplate = isTemplate;
    }

    public Integer getCountrySetting() {
        return mCountrySetting;
    }

    public void setCountrySetting(Integer countrySetting) {
        mCountrySetting = countrySetting;
    }

    public String getCustomColor() {
        return mCustomColor;
    }

    public void setCustomColor(String customColor) {
        mCustomColor = customColor;
    }

    public Integer getCompanyId() {
        return mCompanyId;
    }

    public void setCompanyId(Integer companyId) {
        mCompanyId = companyId;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String companyName) {
        mCompanyName = companyName;
    }

    public boolean isInactive() {
        return mIsInactive;
    }

    public void setIsInactive(boolean isInactive) {
        mIsInactive = isInactive;
    }

    public double getLat() {
        return mLat;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLon() {
        return mLon;
    }

    public void setLon(double lon) {
        mLon = lon;
    }

    public String getScopeOfWork() {
        return mScopeOfWork;
    }

    public void setScopeOfWork(String scopeOfWork) {
        mScopeOfWork = scopeOfWork;
    }

    public boolean isUngrouped() {
        return mIsUngrouped;
    }

    public void setIsUngrouped(boolean isUngrouped) {
        mIsUngrouped = isUngrouped;
    }

    public Integer getTotalTime() {
        return mTotalTime;
    }

    public void setTotalTime(Integer totalTime) {
        mTotalTime = totalTime;
    }

    public double getTotalMoney() {
        return mTotalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        mTotalMoney = totalMoney;
    }

    public String getProjectNotes() {
        return mProjectNotes;
    }

    public void setProjectNotes(String projectNotes) {
        mProjectNotes = projectNotes;
    }

    public Integer getNumberFolders() {
        return mNumberFolders;
    }

    public void setNumberFolders(Integer numberFolders) {
        mNumberFolders = numberFolders;
    }

    public Integer getNumberImages() {
        return mNumberImages;
    }

    public void setNumberImages(Integer numberImages) {
        mNumberImages = numberImages;
    }

    public Integer getNumberAlbums() {
        return mNumberAlbums;
    }

    public void setNumberAlbums(Integer numberAlbums) {
        mNumberAlbums = numberAlbums;
    }

    public Integer getNumberFiles() {
        return mNumberFiles;
    }

    public void setNumberFiles(Integer numberFiles) {
        mNumberFiles = numberFiles;
    }

    public Integer getNumberComments() {
        return mNumberComments;
    }

    public void setNumberComments(Integer numberComments) {
        mNumberComments = numberComments;
    }

    public String getExt() {
        return mExt;
    }

    public void setExt(String ext) {
        mExt = ext;
    }

    public Integer getSize() {
        return mSize;
    }

    public void setSize(Integer size) {
        mSize = size;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getParentName() {
        return mParentName;
    }

    public void setParentName(String parentName) {
        mParentName = parentName;
    }

    public Integer getProjectId() {
        return mProjectId;
    }

    public void setProjectId(Integer projectId) {
        mProjectId = projectId;
    }

    public String getProjectName() {
        return mProjectName;
    }

    public void setProjectName(String projectName) {
        mProjectName = projectName;
    }

    public String getTinyUrl() {
        return mTinyUrl;
    }

    public void setTinyUrl(String tinyUrl) {
        mTinyUrl = tinyUrl;
    }

    public String getMd5() {
        return mMd5;
    }

    public void setMd5(String md5) {
        mMd5 = md5;
    }

    public boolean isDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        mIsDeleted = isDeleted;
    }

    public boolean isAlbum() {
        return mIsAlbum;
    }

    public void setIsAlbum(boolean isAlbum) {
        mIsAlbum = isAlbum;
    }

    public Integer getNumberOfChildren() {
        return mNumberOfChildren;
    }

    public void setNumberOfChildren(Integer numberOfChildren) {
        mNumberOfChildren = numberOfChildren;
    }

    public boolean isFolder() {
        return mIsFolder;
    }

    public void setIsFolder(boolean isFolder) {
        mIsFolder = isFolder;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public boolean isRecentAttachment() {
        return mIsRecentAttachment;
    }

    public void setIsRecentAttachment(boolean isRecentAttachment) {
        mIsRecentAttachment = isRecentAttachment;
    }

    public boolean isPermAttachment() {
        return mIsPermAttachment;
    }

    public void setIsPermAttachment(boolean isPermAttachment) {
        mIsPermAttachment = isPermAttachment;
    }

    public boolean isPrivate() {
        return mIsPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        mIsPrivate = isPrivate;
    }

    public boolean isClientFolder() {
        return mIsClientFolder;
    }

    public void setIsClientFolder(boolean isClientFolder) {
        mIsClientFolder = isClientFolder;
    }

    public boolean isClientUploadFolder() {
        return mIsClientUploadFolder;
    }

    public void setIsClientUploadFolder(boolean isClientUploadFolder) {
        mIsClientUploadFolder = isClientUploadFolder;
    }

    public boolean isSubConFolder() {
        return mIsSubConFolder;
    }

    public void setIsSubConFolder(boolean isSubConFolder) {
        mIsSubConFolder = isSubConFolder;
    }

    public boolean isSharedWithClients() {
        return mIsSharedWithClients;
    }

    public void setIsSharedWithClients(boolean isSharedWithClients) {
        mIsSharedWithClients = isSharedWithClients;
    }

    public boolean isSharedWithSubs() {
        return mSharedWithSubs;
    }

    public void setSharedWithSubs(boolean sharedWithSubs) {
        mSharedWithSubs = sharedWithSubs;
    }

    public boolean isOffline() {
        return mIsOffline;
    }

    public void setIsOffline(boolean isOffline) {
        mIsOffline = isOffline;
    }

    public String getProjectType() {
        return mProjectType;
    }

    public void setProjectType(String projectType) {
        mProjectType = projectType;
    }

    public Integer getContactId() {
        return mContactId;
    }

    public void setContactId(Integer contactId) {
        mContactId = contactId;
    }

    public void setChildren(List<Integer> children) {
        mChildren = children;
    }

    public Date getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        mDateCreated = dateCreated;
    }

    public Date getDateTaken() {
        return mDateTaken;
    }

    public void setDateTaken(Date dateTaken) {
        mDateTaken = dateTaken;
    }

    public Date getLastAccessed() {
        return mLastAccessed;
    }

    public void setLastAccessed(Date lastAccessed) {
        mLastAccessed = lastAccessed;
    }
}
