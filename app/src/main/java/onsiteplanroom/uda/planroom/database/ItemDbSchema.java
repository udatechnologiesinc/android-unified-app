package onsiteplanroom.uda.planroom.database;

/**
 * Created by Harry on 2/2/2016.
 * This class defines what the tables will contain in the database.
 */
public class ItemDbSchema {
    public static final class ContactTable {
        public static final String NAME = "contacts";

        public static final class Cols {
            public static final String ID = "id_contact";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME = "last_name";
            public static final String ROLE = "role";
            public static final String CONTACT_MODIFIED_DATE = "contact_modified_date";
            public static final String EMAIL = "email";
            public static final String ACCOUNT = "account";
            public static final String DISPLAY_NAME = "display_name";
            public static final String COMPANY = "company";
            public static final String ADDRESS = "address";
            public static final String CITY = "city";
            public static final String STATE = "state";
            public static final String ZIP = "zip";
            public static final String PHONE = "phone";
            public static final String MOBILE_PHONE = "mobile_phone";
            public static final String MOBILE_PROVIDER = "mobile_provider";
            public static final String FAX = "fax";
            public static final String LAST_MODIFIED_CONTACT_ID = "contact_id";
            public static final String LAST_MODIFIED_NAME = "last_modified_name";
            public static final String IS_COMPANY = "is_company";
            public static final String MIDDLE_NAME = "middle_name";
            public static final String WEBSITE = "website";
            public static final String SUMMARY = "summary";
            public static final String EXPERIENCE = "experience";
            public static final String CREATOR_ID = "creator_id";
            public static final String CREATOR_NAME = "creator_name";
            public static final String TIMEZONE_OFFSET = "timezone_offset";
            public static final String AUTO_TIME_ZONE = "auto_time_zone";
            public static final String COMPANY_ID = "company_id";
            public static final String COMPANY_NAME = "company_name";
            public static final String COUNTRY_SETTING = "country_setting";
            public static final String LANGUAGE_SETTING = "language_setting";
            public static final String LAT = "lat";
            public static final String LON = "lon";
            public static final String COMPANY_PERMSSIONS = "company_permissions";
            public static final String IS_COMPANY_ADMIN = "is_company_admin";
            public static final String PREMIUM = "premium";
            public static final String MOBILE_TOKENS = "mobile_tokens";
            public static final String SUBSCRIPTION = "subscription";
            public static final String PREMIUM_TYPE = "premium_type";
            public static final String FIELD = "field";

        }
    }

    public static final class FolderFileTable {
        public static final String NAME = "files";

        public static final class Cols {
            public static final String ID = "id_folderfile";
            public static final String PARENT = "parent";
            public static final String CHILDREN = "children";
            public static final String FILE_NAME = "file_name";
            public static final String FILE_MODIFIED_DATE = "file_modified_date";
            public static final String IS_PROJECT = "is_project";
            public static final String IS_PHOTO = "is_photo";
            public static final String FILE_PATH = "file_path";
            public static final String IS_VIDEO = "is_video";
            public static final String CREATED_BY = "created_by";
            public static final String CREATOR_NAME = "creator_name";
            public static final String ADDRESS = "address";
            public static final String CITY = "city";
            public static final String STATE = "state";
            public static final String ZIP = "zip";
            public static final String PROJECT_NUMBER = "project_number";
            public static final String LAST_MODIFIED_CON_ID = "last_mod_con_id";
            public static final String LAST_MODIFIED_NAME = "last_mod_name";
            public static final String PROJECT_TYPE = "project_type";
            public static final String CONTACT_ID = "contact_id";
            public static final String CONTACT_NAME = "contact_name";
            public static final String IS_ARCHIVED = "is_archived";
            public static final String IS_CANADIAN = "is_canadian";
            public static final String CURRENCY = "currency";
            public static final String IS_TEMPLATE = "is_template";
            public static final String COUNTRY_SETTING = "country_setting";
            public static final String CUSTOM_COLOR = "custom_color";
            public static final String COMPANY_ID = "company_id";
            public static final String COMPANY_NAME = "company_name";
            public static final String IS_INACTIVE = "is_inactive";
            public static final String LAT = "lat";
            public static final String LON = "lon";
            public static final String SCOPE_OF_WORK = "scope_of_work";
            public static final String IS_UNGROUPED = "is_ungrouped";
            public static final String TOTAL_TIME = "total_time";
            public static final String TOTAL_MONEY = "total_money";
            public static final String PROJECT_NOTES = "project_notes";
            public static final String NUMBER_FOLDERS = "number_folders";
            public static final String NUMBER_IMAGES = "number_images";
            public static final String NUMBER_ALBUMS = "number_albums";
            public static final String NUMBER_FILES = "number_files";
            public static final String EXT = "ext";
            public static final String SIZE = "size";
            public static final String TYPE = "type";
            public static final String PARENT_NAME = "parent_name";
            public static final String PROJECT_ID = "project_id";
            public static final String PROJECT_NAME = "project_name";
            public static final String TINY_URL = "tiny_url";
            public static final String MD5 = "md5";
            public static final String IS_DELETED = "is_deleted";
            public static final String IS_ALBUM = "is_album";
            public static final String NUMBER_CHILDREN = "number_children";
            public static final String IS_FOLDER ="is_folder";
            public static final String NUMBER_COMMENTS = "number_comments";
            public static final String CAPTION = "caption";
            public static final String FULL_NAME = "full_name";
            public static final String IS_RECENT_ATTACHMENT = "is_recent_attachment";
            public static final String IS_PERM_ATTACHMENT = "is_perm_attachment";
            public static final String IS_PRIVATE = "is_private";
            public static final String IS_CLIENT_FOLDER = "is_client_folder";
            public static final String IS_CLIENT_UPLOAD_FOLDER = "is_client_upload_folder";
            public static final String IS_SHARED_WITH_CLIENTS = "is_shared_with_clients";
            public static final String IS_SHARED_WITH_SUBS = "is_shared_with_subs";
            public static final String IS_OFFLINE = "is_offline";
            public static final String DATE_CREATED = "date_created";
            public static final String DATE_TAKEN = "date_taken";
            public static final String LAST_ACCESSED = "last_accessed";
        }
    }
}
