package onsiteplanroom.uda.planroom.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Harry on 2/2/2016.
 * This is the database helper for the two tables (Contact and FolderFile tables)
 */
public class ItemBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 2;
    private static final String DATABASE_NAME = "itemBase.db";

    public ItemBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + ItemDbSchema.FolderFileTable.NAME
                        + "(" + "_id integer primary key autoincrement,"
                        + ItemDbSchema.FolderFileTable.Cols.ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.PARENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.CHILDREN + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_MODIFIED_DATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PROJECT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PHOTO + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_PATH + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_VIDEO + ","
                        + ItemDbSchema.FolderFileTable.Cols.CREATED_BY + ","
                        + ItemDbSchema.FolderFileTable.Cols.CREATOR_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.ADDRESS + ","
                        + ItemDbSchema.FolderFileTable.Cols.CITY + ","
                        + ItemDbSchema.FolderFileTable.Cols.STATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.ZIP + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_CON_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_TYPE + ","
                        + ItemDbSchema.FolderFileTable.Cols.CONTACT_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.CONTACT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_ARCHIVED + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CANADIAN + ","
                        + ItemDbSchema.FolderFileTable.Cols.CURRENCY + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_TEMPLATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.COUNTRY_SETTING + ","
                        + ItemDbSchema.FolderFileTable.Cols.CUSTOM_COLOR + ","
                        + ItemDbSchema.FolderFileTable.Cols.COMPANY_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.COMPANY_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_INACTIVE + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAT + ","
                        + ItemDbSchema.FolderFileTable.Cols.LON + ","
                        + ItemDbSchema.FolderFileTable.Cols.SCOPE_OF_WORK + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_UNGROUPED + ","
                        + ItemDbSchema.FolderFileTable.Cols.TOTAL_TIME + ","
                        + ItemDbSchema.FolderFileTable.Cols.TOTAL_MONEY + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NUMBER + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NOTES + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_FOLDERS + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_IMAGES + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_ALBUMS + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_FILES + ","
                        + ItemDbSchema.FolderFileTable.Cols.EXT + ","
                        + ItemDbSchema.FolderFileTable.Cols.SIZE + ","
                        + ItemDbSchema.FolderFileTable.Cols.TYPE + ","
                        + ItemDbSchema.FolderFileTable.Cols.PARENT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.TINY_URL + ","
                        + ItemDbSchema.FolderFileTable.Cols.MD5 + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_DELETED + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_ALBUM + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_CHILDREN + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_COMMENTS + ","
                        + ItemDbSchema.FolderFileTable.Cols.CAPTION + ","
                        + ItemDbSchema.FolderFileTable.Cols.FULL_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_RECENT_ATTACHMENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PERM_ATTACHMENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PRIVATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_UPLOAD_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_CLIENTS + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_SUBS + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_OFFLINE + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_ACCESSED + ","
                        + ItemDbSchema.FolderFileTable.Cols.DATE_CREATED + ","
                        + ItemDbSchema.FolderFileTable.Cols.DATE_TAKEN
                        + ")"
        );
        db.execSQL("create table " + ItemDbSchema.ContactTable.NAME
                        + "(" + "_id integer primary key autoincrement,"
                        + ItemDbSchema.ContactTable.Cols.ID + ","
                        + ItemDbSchema.ContactTable.Cols.FIRST_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.ROLE + ","
                        + ItemDbSchema.ContactTable.Cols.CONTACT_MODIFIED_DATE + ","
                        + ItemDbSchema.ContactTable.Cols.EMAIL + ","
                        + ItemDbSchema.ContactTable.Cols.ACCOUNT + ","
                        + ItemDbSchema.ContactTable.Cols.DISPLAY_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY + ","
                        + ItemDbSchema.ContactTable.Cols.ADDRESS + ","
                        + ItemDbSchema.ContactTable.Cols.CITY + ","
                        + ItemDbSchema.ContactTable.Cols.STATE + ","
                        + ItemDbSchema.ContactTable.Cols.ZIP + ","
                        + ItemDbSchema.ContactTable.Cols.PHONE + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_PHONE + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_PROVIDER + ","
                        + ItemDbSchema.ContactTable.Cols.FAX + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_CONTACT_ID + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.IS_COMPANY + ","
                        + ItemDbSchema.ContactTable.Cols.MIDDLE_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.WEBSITE + ","
                        + ItemDbSchema.ContactTable.Cols.SUMMARY + ","
                        + ItemDbSchema.ContactTable.Cols.EXPERIENCE + ","
                        + ItemDbSchema.ContactTable.Cols.CREATOR_ID + ","
                        + ItemDbSchema.ContactTable.Cols.CREATOR_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.TIMEZONE_OFFSET + ","
                        + ItemDbSchema.ContactTable.Cols.AUTO_TIME_ZONE + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_ID + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.COUNTRY_SETTING + ","
                        + ItemDbSchema.ContactTable.Cols.LANGUAGE_SETTING + ","
                        + ItemDbSchema.ContactTable.Cols.LAT + ","
                        + ItemDbSchema.ContactTable.Cols.LON + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_PERMSSIONS + ","
                        + ItemDbSchema.ContactTable.Cols.IS_COMPANY_ADMIN + ","
                        + ItemDbSchema.ContactTable.Cols.PREMIUM + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_TOKENS + ","
                        + ItemDbSchema.ContactTable.Cols.SUBSCRIPTION + ","
                        + ItemDbSchema.ContactTable.Cols.PREMIUM_TYPE + ","
                        + ItemDbSchema.ContactTable.Cols.FIELD
                        + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        //
    }
}
