package onsiteplanroom.uda.planroom.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.Objects.FolderFile;

/**
 * Created by Harry on 2/2/2016.
 * This class is what is called when something needs to be retrieved from the database.
 */
public class ItemCursorWrapper extends CursorWrapper {
    public ItemCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /** Gets a row from the database, creates an object, and puts it in the lab.
     * @return a contact object from the database.
     */
    public Contact getContact() {
        String id = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.ID
        ));
        String firstName = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.FIRST_NAME
        ));
        String lastName = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.LAST_NAME
        ));
        String role = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.ROLE
        ));
        long dateModified = getLong(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.CONTACT_MODIFIED_DATE
        ));
        String email = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.EMAIL
        ));
        String account = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.ACCOUNT
        ));
        String displayName = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.DISPLAY_NAME
        ));
        String company = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.COMPANY
        ));
        String address = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.ADDRESS
        ));
        String city = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.CITY
        ));
        String state = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.STATE
        ));
        String zip = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.ZIP
                ));
        String phone = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.PHONE
        ));
        String mobilePhone = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.MOBILE_PHONE
        ));
        String mobileProvider = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.MOBILE_PROVIDER
                ));
        String fax = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.FAX
        ));
        int lModConId = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_CONTACT_ID
        ));
        String lModName = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_NAME
        ));
        int isCompany = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.IS_COMPANY
        ));
        String middleName = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.MIDDLE_NAME
        ));
        String website = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.WEBSITE
        ));
        String summary = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.SUMMARY
        ));
        String experience = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.EXPERIENCE
        ));
        int creatorId = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.CREATOR_ID
        ));
        String creatorName = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.CREATOR_NAME
        ));
        double timezoneOffset = getDouble(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.TIMEZONE_OFFSET
        ));
        int autoTimezone = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.AUTO_TIME_ZONE
        ));
        int companyId = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.COMPANY_ID
        ));
        String companyName = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.COMPANY_NAME
        ));
        int countrySetting = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.COUNTRY_SETTING));
        int languageSetting = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.LANGUAGE_SETTING));
        double lat = getDouble(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.LAT));
        double lon = getDouble(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.LON));
        int companyPermissions = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.COMPANY_PERMSSIONS));
        int isCompanyAdmin = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.IS_COMPANY_ADMIN));
        String premium = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.PREMIUM));
        String mobileTokens = getString(getColumnIndex(
                        ItemDbSchema.ContactTable.Cols.MOBILE_TOKENS));
        String subscription = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.SUBSCRIPTION));
        int premiumType = getInt(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.PREMIUM_TYPE));
        String field = getString(getColumnIndex(
                ItemDbSchema.ContactTable.Cols.FIELD));


        Contact contact = new Contact(Integer.parseInt(id));
        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setCompanyName(companyName);
        contact.setRole(role);
        contact.setDateModified(new Date(dateModified));
        contact.setEmail(email);
        contact.setAccount(account);
        contact.setDisplayName(displayName);
        contact.setCompany(company);
        contact.setAddress(address);
        contact.setCity(city);
        contact.setState(state);
        contact.setZip(zip);
        contact.setPhone(phone);
        contact.setMobilePhone(mobilePhone);
        contact.setMobileProvider(mobileProvider);
        contact.setFax(fax);
        contact.setLastModifiedContactId(lModConId);
        contact.setLastModifiedname(lModName);
        contact.setIsCompany(isCompany != 0);
        contact.setMiddleName(middleName);
        contact.setWebsite(website);
        contact.setSummary(summary);
        contact.setExperience(experience);
        contact.setCreatorId(creatorId);
        contact.setCreatorName(creatorName);
        contact.setTimezoneOffset(timezoneOffset);
        contact.setAutoTimezone(autoTimezone != 0);
        contact.setCompanyId(companyId);
        contact.setCountrySetting(countrySetting);
        contact.setLanguageSetting(languageSetting);
        contact.setLat(lat);
        contact.setLon(lon);
        contact.setCompanyPermissions(companyPermissions);
        contact.setIsCompanyAdmin(isCompanyAdmin != 0);
        contact.setPremium(premium);
        contact.setMobileTokens(mobileTokens);
        contact.setSubscription(subscription);
        contact.setPremiumType(premiumType);
        contact.setField(field);



        return contact;
    }

    /** Loads the FolderFile data into the lab.
     * @return a FolderFile object with the data from a particular row
     */

    public FolderFile getFolderFile() {
        String id = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.ID
        ));

        String parent = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PARENT
        ));
        String children = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CHILDREN
        ));

        String fileName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.FILE_NAME
        ));
        long fileModifiedDate = getLong(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.FILE_MODIFIED_DATE
        ));
        int isProject = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_PROJECT
        ));
        int isPhoto = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_PHOTO
        ));
        String filePath = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.FILE_PATH
        ));
        int isVideo = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_VIDEO
        ));
        int createdBy = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CREATED_BY
        ));
        String creatorName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CREATOR_NAME));
        String address = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.ADDRESS));
        String city = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CITY));
        String state = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.STATE));
        String zip = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.ZIP));
        String projectNumber = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PROJECT_NUMBER));
        int lmodConId = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_CON_ID));
        String lmodName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_NAME));
        String projectType = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PROJECT_TYPE));
        int contactId = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CONTACT_ID));
        String contactName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CONTACT_NAME));
        int isArchived = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_ARCHIVED));
        int isCanadian = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_CANADIAN));
        int currency = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CURRENCY));
        int isTemplate = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_TEMPLATE));
        int countrySetting = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.COUNTRY_SETTING));
        String customColor = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CUSTOM_COLOR));
        int companyId = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.COMPANY_ID));
        String companyName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.COMPANY_NAME));
        int isInactive = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_INACTIVE));
//        double lat = getDouble(getColumnIndex(
//                ItemDbSchema.FolderFileTable.Cols.LAT));
//        double lon = getDouble(getColumnIndex(
//                ItemDbSchema.FolderFileTable.Cols.LON));
        String scopeOfWork = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.SCOPE_OF_WORK));
        int isUngrouped = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_UNGROUPED));
        int totalTime = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.TOTAL_TIME));
        int totalMoney = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.TOTAL_MONEY));
        String projectNotes = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PROJECT_NOTES));
        int numberFolders = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_FOLDERS));
        int numberImages = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_IMAGES));
        int numberAlbums = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_ALBUMS));
        int numberFiles = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_FILES));
        int numberComments = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_COMMENTS));
        String ext = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.EXT));
        int size = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.SIZE));
        String type = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.TYPE));
        String parentName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PARENT_NAME));
        int projectId = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PROJECT_ID));
        String projectName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.PROJECT_NAME));
        String tinyUrl = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.TINY_URL));
        String md5 = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.MD5));
        int isDeleted = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_DELETED));
        int isAlbum = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_ALBUM));
        int numberChildren = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.NUMBER_CHILDREN));
        int isFolder = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_FOLDER));
        String caption = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.CAPTION));
        String fullName = getString(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.FULL_NAME));
        int isRecentAttachment = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_RECENT_ATTACHMENT));
        int isPermAttachment = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_PERM_ATTACHMENT));
        int isPrivate = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_PRIVATE));
        int isClientFolder = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_FOLDER));
        int isClientUploadFolder = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_UPLOAD_FOLDER));
        int isSharedWithClients = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_CLIENTS));
        int isOffline = getInt(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.IS_OFFLINE));
        long dateCreated = getLong(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.DATE_CREATED
        ));
        long dateTaken = getLong(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.DATE_TAKEN
        ));
        long lastAccessed = getLong(getColumnIndex(
                ItemDbSchema.FolderFileTable.Cols.LAST_ACCESSED
        ));



        FolderFile file = new FolderFile(Integer.parseInt(id));
        file.setCreatedBy(createdBy);
        file.setCreatorName(creatorName);

        String delims = "[,]";
        String[] childrenStringArray = children.split(delims);
        List childrenList = new ArrayList();

        for (int i = 0; i < childrenStringArray.length; i++) {
            if (!childrenStringArray[i].equals("")) {
                childrenList.add(Integer.parseInt(childrenStringArray[i]));
            }
        }
        file.setChildren(childrenList);

        file.setAddress(address);
        file.setCity(city);
        file.setState(state);
        file.setZip(zip);
        file.setProjectNumber(projectNumber);
        file.setLastModifiedContactId(lmodConId);
        file.setLastModifiedName(lmodName);
        file.setProjectType(projectType);
        file.setContactId(contactId);
        file.setContactName(contactName);
        file.setIsArchived(isArchived != 0);
        file.setIsCanadian(isCanadian != 0);
        file.setCurrency(currency);
        file.setIsTemplate(isTemplate != 0);
        file.setCountrySetting(countrySetting);
        file.setCustomColor(customColor);
        file.setCompanyId(companyId);
        file.setCompanyName(companyName);
        file.setIsInactive(isInactive != 0);
//        file.setLat(lat);
//        file.setLon(lon);
        file.setScopeOfWork(scopeOfWork);
        file.setIsUngrouped(isUngrouped != 0);
        file.setTotalTime(totalTime);
        file.setTotalMoney(totalMoney);
        file.setProjectNotes(projectNotes);
        file.setNumberFolders(numberFolders);
        file.setNumberFiles(numberFiles);
        file.setNumberImages(numberImages);
        file.setNumberAlbums(numberAlbums);
        file.setNumberComments(numberComments);
        file.setExt(ext);
        file.setSize(size);
        file.setType(type);
        file.setParentName(parentName);
        file.setProjectId(projectId);
        file.setProjectName(projectName);
        file.setTinyUrl(tinyUrl);
        file.setMd5(md5);
        file.setIsDeleted(isDeleted != 0);
        file.setIsAlbum(isAlbum != 0);
        file.setNumberOfChildren(numberChildren);
        file.setIsFolder(isFolder != 0);
        file.setCaption(caption);
        file.setFullName(fullName);
        file.setIsRecentAttachment(isRecentAttachment != 0);
        file.setIsPermAttachment(isPermAttachment != 0);
        file.setIsPrivate(isPrivate != 0);
        file.setIsClientFolder(isClientFolder != 0);
        file.setIsClientUploadFolder(isClientUploadFolder != 0);
        file.setIsSharedWithClients(isSharedWithClients != 0);
        file.setIsOffline(isOffline != 0);


        file.setIsProject(isProject != 0);
        file.setIsPhoto(isPhoto != 0);
        file.setIsVideo(isVideo != 0);

        file.setParent(Integer.parseInt(parent));

        file.setName(fileName);
        file.setModifiedDate(new Date(fileModifiedDate));
        file.setLastAccessed(new Date(lastAccessed));
        file.setDateCreated(new Date(dateCreated));
        file.setDateTaken(new Date(dateTaken));
        file.setFilePath(filePath);


        return file;
    }

}




















