package onsiteplanroom.uda.planroom.WebServices;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.UI.ContactsListFragment;
import onsiteplanroom.uda.planroom.labs.ContactLab;

/**
 * Created by Harry on 3/31/2016.
 * <p/>
 * This class does a refresh for the ContactListFragment. The refreshContacts() initiates the
 * download chain. The last refresh date is needed to start the chain. This is done to reduce the
 * refresh times dramatically.
 */

public class RefreshContacts {

    /**
     * Starts the download chain.
     *
     * @param context            context for lab.
     * @param username           username needed for authentication.
     * @param password           password needed for authenticatiion.
     * @param fromDate           last time user refreshed.
     * @param fragment           the ContactListFragment instance so the UI can refresh when finished pulling down data.
     * @param swipeRefreshLayout The SwipeRefreshLayout instance so the refresh animation can finish when finished pulling down data.
     */

    public static void refreshContacts(Context context, String username,
                                       String password, String fromDate,
                                       ContactsListFragment fragment,
                                       SwipeRefreshLayout swipeRefreshLayout) {
        String contact_date = fromDate;

        if (contact_date == null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    "user_pref", Context.MODE_PRIVATE);

            contact_date = prefs.getString("contacts_last_refreshed", null);
        }

        refreshNewEditedContacts(context, username, password, contact_date, fragment,
                swipeRefreshLayout);
    }

    /**
     * Refreshes contacts that are either new or have been modified since last refresh.
     *
     * @param context            context for lab.
     * @param _username          username needed for authentication.
     * @param _password          needed for authenticatiion.
     * @param fromDate           last time user refreshed.
     * @param fragment           the ContactListFragment instance so the UI can refresh when finished pulling down data.
     * @param swipeRefreshLayout The SwipeRefreshLayout instance so the refresh animation can finish when finished pulling down data.
     */

    public static void refreshNewEditedContacts(final Context context, String _username,
                                                String _password, final String fromDate,
                                                final ContactsListFragment fragment,
                                                final SwipeRefreshLayout swipeRefreshLayout) {
        final String username = _username;
        final String password = _password;
        final String url = "http://www.constructiononline.com:8080/"
                + "api/Contacts?fromDate=" + fromDate;
        final String charset = "UTF-8";
        final ContactsListFragment contactsListFragment = fragment;
        final SwipeRefreshLayout refreshLayout = swipeRefreshLayout;


        AsyncTask<Void, Void, Void> downloadTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(username, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "&" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();

                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            ContactLab lab = ContactLab.get(context);

                            Contact contact = new Contact(object.getInt("ID"));
                            contact.setFirstName(object.getString("FIRSTNAME"));
                            contact.setLastName(object.getString("LASTNAME"));
                            contact.setEmail(object.getString("EMAIL"));
                            contact.setAccount(object.getString("ACCOUNT"));
                            contact.setDisplayName(object.getString("DISPLAY_NAME"));
                            contact.setCompany(object.getString("COMPANY"));
                            contact.setAddress(object.getString("ADDRESS"));
                            contact.setCity(object.getString("CITY"));
                            contact.setState(object.getString("STATE"));
                            contact.setZip(object.getString("ZIP"));
                            contact.setPhone(object.getString("PHONE"));
                            contact.setMobilePhone(object.getString("MOBILE_PHONE"));
                            contact.setMobileProvider(object.getString("MOBILE_PROVIDER"));
                            contact.setFax(object.getString("FAX"));
                            contact.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            contact.setLastModifiedname(object.getString("LMOD_NAME"));
                            contact.setIsCompany(object.getBoolean("ISCOMPANY"));
                            contact.setMiddleName(object.getString("MIDDLENAME"));
                            contact.setWebsite(object.getString("WEBSITE"));
                            contact.setSummary(object.getString("SUMMARY"));
                            contact.setExperience(object.getString("EXPERIENCE"));
                            //contact.setCreatorId(object.getInt("CREATOR_ID"));
                            contact.setCreatorName(object.getString("CREATOR_NAME"));
                            contact.setTimezoneOffset(object.getDouble("TIMEZONE_OFFSET"));
                            contact.setAutoTimezone(object.getBoolean("AUTO_TIMEZONE"));
                            //contact.setCompanyId(object.getInt("COMPANY_ID"));
                            contact.setCompanyName(object.getString("COMPANY_NAME"));
                            contact.setCountrySetting(object.getInt("COUNTRY_SETTING"));
                            //contact.setLanguageSetting(object.getInt("LANGUAGE_SETTING"));
//                            contact.setLat(object.getDouble("LAT"));
//                            contact.setLon(object.getDouble("LON"));
                            contact.setCompanyPermissions(object.getInt("COMPANY_PERMISSIONS"
                            ));
                            contact.setIsCompanyAdmin(object.getBoolean("IS_COMPANY_ADMIN"));
                            contact.setPremium(object.getString("PREMIUM"));
                            contact.setMobileTokens(object.getString("MOBILETOKENS"));
                            contact.setSubscription(object.getString("SUBSCRIPTION"));
                            contact.setPremiumType(object.getInt("PREMIUM_TYPE"));
                            contact.setField(object.getString("FIELD"));


                            Contact testExistance = lab.getContact(contact.getId());
                            if (testExistance == null) {
                                lab.addContact(contact);
                            } else {
                                lab.deleteContact(testExistance);
                                lab.addContact(contact);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void voidParam) {


                refreshLayout.setRefreshing(false);
                contactsListFragment.updateUI();

            }
        };

        downloadTask.execute();
    }
}
