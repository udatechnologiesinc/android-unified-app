package onsiteplanroom.uda.planroom.WebServices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.R;
import onsiteplanroom.uda.planroom.UI.MainActivity;
import onsiteplanroom.uda.planroom.labs.ContactLab;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/31/2016.
 * <p/>
 * This class contains the webservices for the INITIAL download of all the contacts and projects.
 * The method downloadAll is used to iniate the download chain for each method. The next method
 * is called in the onPostExecute to ensure that it finished the AsyncTask it had running.
 * <p/>
 * This class has potential to change when adding new features. I may just load data as needed.
 */


public class InitialDownload {

    /**
     * Initiates download chain.
     *
     * @param context   context to access lab
     * @param _username current user for authentication
     * @param _password current password for authentication
     */

    public static void downloadAll(Context context, String _username, String _password) {
        downloadContacts(context, _username, _password);
    }


    /**
     * Downloads all the Projects.
     *
     * @param _context  context to access lab
     * @param _userName current user for authentication
     * @param _password current password for authentication
     */
    public static void downloadProjects(Context _context, String _userName, String _password) {

        final Context context = _context;
        final String username = _userName;
        final String password = _password;
        final String charset = "UTF-8";
        final String url = "http://www.constructiononline.com:8080/api/Projects";
        final Activity activity = (Activity) _context;

        AsyncTask<Void, Void, Void> downloadTask = new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(username, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "?" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();
                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            FolderFileLab lab = FolderFileLab.get(context);

                            FolderFile file = new FolderFile(object.getInt("ID"));
                            file.setIsProject(true);
                            file.setName(object.getString("NAME"));
                            file.setParent(-1);
                            file.setCreatedBy(object.getInt("CREATED_BY"));
                            file.setCreatorName(object.getString("CREATOR_NAME"));
                            file.setAddress(object.getString("ADDRESS"));
                            file.setCity(object.getString("CITY"));
                            file.setState(object.getString("STATE"));
                            file.setZip(object.getString("ZIP"));
                            file.setProjectNumber(object.getString("PROJECT_NUMBER"));
                            file.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            file.setLastModifiedName(object.getString("LMOD_NAME"));
                            file.setProjectType(object.getString("PROJECT_TYPE"));
                            file.setContactId(object.getInt("CON_ID"));
                            file.setContactName(object.getString("CON_NAME"));
                            file.setIsArchived(object.getBoolean("IS_ARCHIVED"));
                            file.setIsCanadian(object.getBoolean("IS_CANADIAN"));
                            file.setCurrency(object.getInt("CURRENCY"));
                            file.setIsTemplate(object.getBoolean("IS_TEMPLATE"));
                            file.setCountrySetting(object.getInt("COUNTRY_SETTING"));
                            file.setCustomColor(object.getString("CUSTOM_COLOR"));
                            file.setCompanyName(object.getString("COMPANY_NAME"));
                            file.setIsInactive(object.getBoolean("IS_INACTIVE"));
//                            file.setLat(object.getDouble("LAT"));
//                            file.setLon(object.getDouble("LON"));
                            file.setScopeOfWork(object.getString("SCOPE_OF_WORK"));
                            file.setIsUngrouped(object.getBoolean("IS_UNGROUPED"));
                            file.setTotalTime(object.getInt("TOTAL_TIME"));
                            file.setTotalMoney(object.getInt("TOTAL_MONEY"));
                            file.setProjectNotes(object.getString("PROJECT_NOTES"));
                            file.setNumberFolders(object.getInt("NUMBERFOLDERS"));
                            file.setNumberImages(object.getInt("NUMBERIMAGES"));
                            file.setNumberAlbums(object.getInt("NUMBERALBUMS"));
                            file.setNumberFiles(object.getInt("NUMBERFILES"));
                            file.setNumberComments(object.getInt("NUMBEROFCOMMENTS"));


                            String modifiedDateStr = object.getString("LMOD");
                            String createdDateStr = object.getString("DATE_CREATED");


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");
                            try {
                                Date birthDate = df.parse(modifiedDateStr);
                                file.setModifiedDate(birthDate);

                                Date createdDate = df.parse(createdDateStr);
                                file.setDateCreated(createdDate);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                            lab.addFolderFile(file);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }


            @Override
            protected void onPostExecute(Void result) {


                SharedPreferences prefs = activity.getSharedPreferences(
                        "user_pref", Context.MODE_PRIVATE);
                String mUser = prefs.getString("user_pref", null);
                String mPassword = prefs.getString("password_pref", null);


                FolderFileLab lab = FolderFileLab.get(context);
                List<FolderFile> files = lab.getProjects();

                int size = files.size();

                for (FolderFile file : files) {
                    if (files.indexOf(file) != (size - 1)) {

                        downloadFiles(context, mUser, mPassword, file.getId(),
                                false, true);
                    } else {
                        downloadFiles(context, mUser, mPassword, file.getId(),
                                true, true);
                    }
                }
            }

        };
        downloadTask.execute();
    }


    /**
     * @param context   context to access lab
     * @param _username current user for authentication
     * @param _password current password for authentication
     */

    public static void downloadContacts(final Context context, String _username, String _password) {

        final String username = _username;
        final String password = _password;
        final String url = "http://www.constructiononline.com:8080/api/Contacts";
        final String charset = "UTF-8";

        AsyncTask<Void, Void, Void> downloadTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(username, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "?" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();

                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            ContactLab lab = ContactLab.get(context);

                            Contact contact = new Contact(object.getInt("ID"));
                            contact.setFirstName(object.getString("FIRSTNAME"));
                            contact.setLastName(object.getString("LASTNAME"));
                            contact.setEmail(object.getString("EMAIL"));
                            contact.setAccount(object.getString("ACCOUNT"));
                            contact.setDisplayName(object.getString("DISPLAY_NAME"));
                            contact.setCompany(object.getString("COMPANY"));
                            contact.setAddress(object.getString("ADDRESS"));
                            contact.setCity(object.getString("CITY"));
                            contact.setState(object.getString("STATE"));
                            contact.setZip(object.getString("ZIP"));
                            contact.setPhone(object.getString("PHONE"));
                            contact.setMobilePhone(object.getString("MOBILE_PHONE"));
                            contact.setMobileProvider(object.getString("MOBILE_PROVIDER"));
                            contact.setFax(object.getString("FAX"));
                            contact.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            contact.setLastModifiedname(object.getString("LMOD_NAME"));
                            contact.setIsCompany(object.getBoolean("ISCOMPANY"));
                            contact.setMiddleName(object.getString("MIDDLENAME"));
                            contact.setWebsite(object.getString("WEBSITE"));
                            contact.setSummary(object.getString("SUMMARY"));
                            contact.setExperience(object.getString("EXPERIENCE"));
                            //contact.setCreatorId(object.getInt("CREATOR_ID"));
                            contact.setCreatorName(object.getString("CREATOR_NAME"));
                            contact.setTimezoneOffset(object.getDouble("TIMEZONE_OFFSET"));
                            contact.setAutoTimezone(object.getBoolean("AUTO_TIMEZONE"));
                            //contact.setCompanyId(object.getInt("COMPANY_ID"));
                            contact.setCompanyName(object.getString("COMPANY_NAME"));
                            contact.setCountrySetting(object.getInt("COUNTRY_SETTING"));
                            //contact.setLanguageSetting(object.getInt("LANGUAGE_SETTING"));
//                            contact.setLat(object.getDouble("LAT"));
//                            contact.setLon(object.getDouble("LON"));
                            contact.setCompanyPermissions(object.getInt("COMPANY_PERMISSIONS"
                            ));
                            contact.setIsCompanyAdmin(object.getBoolean("IS_COMPANY_ADMIN"));
                            contact.setPremium(object.getString("PREMIUM"));
                            contact.setMobileTokens(object.getString("MOBILETOKENS"));
                            contact.setSubscription(object.getString("SUBSCRIPTION"));
                            contact.setPremiumType(object.getInt("PREMIUM_TYPE"));
                            contact.setField(object.getString("FIELD"));


                            lab.addContact(contact);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void voidParam) {
                downloadProjects(context, username, password);
            }

        };

        downloadTask.execute();
    }

    /**
     * Downloads the files of a parent.
     *
     * @param _context   for lab
     * @param _username  user needed for authentication
     * @param _password  password needed for authenticaiton
     * @param _id        project ID
     * @param isLastFile is this the last AsyncTask download for the projects. (Used to know when finished.
     * @param _recurse   recurse = true loads all subfolders for a parent. recurse = false loads just the first level.
     */

    public static void downloadFiles(Context _context,
                                     String _username, String _password, Integer _id,
                                     final boolean isLastFile, boolean _recurse) {
        final Context context = _context;
        final String username = _username;
        final String password = _password;
        final Integer id = _id;
        final String charset = "UTF-8";
        final boolean recurse = _recurse;
        final String url;

        if (recurse) {
            url = "http://www.constructiononline.com:8080/" +
                    "api/Files?projectID=" + id + "&recurse=true";
        } else {
            url = "http://www.constructiononline.com:8080/" +
                    "api/Files?projectID=" + id + "&recurse=false";
        }


        final Activity activity = (Activity) context;


        AsyncTask<Void, Void, List<FolderFile>> downloadTask = new AsyncTask<Void, Void, List<FolderFile>>() {
            private int position;


            @Override
            protected List<FolderFile> doInBackground(Void... params) {

                List<FolderFile> downloadedFiles = new ArrayList<>();
                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(username, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "&" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();
                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            FolderFileLab lab = FolderFileLab.get(context);
                            FolderFile file = new FolderFile(object.getInt("ID"));


                            if (object.isNull("PARENT_ID")) {
                                file.setParent(id);
                            } else {
                                file.setParent(object.getInt("PARENT_ID"));
                            }
                            file.setName(object.getString("NAME"));

                            file.setExt(object.getString("EXT"));
                            file.setSize(object.getInt("SIZE"));
                            file.setType(object.getString("TYPE"));
                            file.setParentName(object.getString("PARENT_NAME"));
                            file.setProjectId(object.getInt("PRJ_ID"));
                            file.setProjectName(object.getString("PROJECT_NAME"));
                            file.setContactId(object.getInt("CON_ID"));
                            file.setContactName(object.getString("CON_NAME"));
                            file.setTinyUrl(object.getString("TINYURL"));
                            file.setMd5(object.getString("MD5"));
                            file.setIsDeleted(object.getBoolean("DELETED"));
                            file.setIsAlbum(object.getBoolean("ISALBUM"));
                            file.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            file.setLastModifiedName(object.getString("LMOD_CONTACT"));
                            file.setIsPhoto(object.getBoolean("ISIMAGE"));
                            file.setNumberOfChildren(object.getInt("NUMBEROFCHILDREN"));
                            file.setIsFolder(object.getBoolean("ISFOLDER"));
                            file.setNumberComments(object.getInt("NUMBEROFCOMMENTS"));
                            file.setCreatedBy(object.getInt("CREATED_BY"));
                            file.setCreatorName(object.getString("CREATOR"));
                            file.setCaption(object.getString("CAPTION"));
                            file.setFullName(object.getString("FULLNAME"));
                            file.setIsRecentAttachment(object.getBoolean("ISRECENTATTACHMENT"));
                            file.setIsPermAttachment(object.getBoolean("ISPERMATTACHMENT"));
                            file.setIsPrivate(object.getBoolean("ISPRIVATE"));
                            file.setIsClientFolder(object.getBoolean("IS_CLIENTFOLDER"));
                            file.setIsClientUploadFolder(object.getBoolean("IS_CLIENT_UPLOAD_FOLDER"));
                            file.setIsSubConFolder(object.getBoolean("IS_SUBCONFOLDER"));
                            //file.setIsSharedWithClients(object.getBoolean("SHARED_WITH_CLIENTS"));
                            //file.setSharedWithSubs(object.getBoolean("SHARED_WITH_SUBS"));

                            String lastAccessedStr = object.getString("LACCESS");
                            String dateTakenStr = object.getString("DATE_TAKEN");
                            String modifiedDateStr = object.getString("LMOD");
                            String createdDateStr = object.getString("DATE_CREATED");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");

                            try {
                                Date birthDate = df.parse(modifiedDateStr);
                                file.setModifiedDate(birthDate);

                                Date createdDate = df.parse(createdDateStr);
                                file.setDateCreated(createdDate);


                                if (!lastAccessedStr.equals("null")) {
                                    Date lastAccessed = df.parse(lastAccessedStr);
                                    file.setLastAccessed(lastAccessed);
                                }

                                if (!dateTakenStr.equals("null")) {
                                    Date dateTaken = df.parse(dateTakenStr);
                                    file.setDateTaken(dateTaken);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            lab.addFolderFile(file);
                            downloadedFiles.add(file);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return downloadedFiles;
            }

            @Override
            protected void onPostExecute(List<FolderFile> result) {

                if (isLastFile) {

                    FolderFileLab lab = FolderFileLab.get(context);
                    List<FolderFile> files = lab.getFolderFiles();

                    for (FolderFile file : files) {
                        if (!file.isProject()) {
                            FolderFile parent = lab.getFolderFile(file.getParent());
                            parent.addChild(file.getId());
                            lab.updateFolderFile(parent);
                        }
                    }

                    Intent intent = new Intent(activity, MainActivity.class);
                    activity.startActivity(intent);
                    activity.finish();

                    Toast toast = new Toast(activity);
                    ImageView view = new ImageView(activity);
                    view.setImageResource(R.mipmap.ic_lock_open_black_36dp);
                    toast.setView(view);
                    toast.show();
                }

            }
        };

        downloadTask.execute();
    }


}
