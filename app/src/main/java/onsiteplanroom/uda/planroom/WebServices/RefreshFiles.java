package onsiteplanroom.uda.planroom.WebServices;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.UI.FilesListFragment;
import onsiteplanroom.uda.planroom.labs.FolderFileLab;

/**
 * Created by Harry on 3/31/2016.
 * This class refreshes the Projects, Folders, and Files in the FileListFragment class.
 * The refreshFolderFiles() starts the download chain. After the last file has been added the
 * UI is updated.
 */


public class RefreshFiles {

    /**
     * Starts the Webservice chain.
     *
     * @param context     context for lab.
     * @param user        user for authentication.
     * @param password    user for authentication.
     * @param date        last refreshed date.
     * @param swipeButton the refresh spinner instance so it can be stopped at the end.
     * @param fragment    the FilesListFragment instance so it cna be updated at the end.
     */

    public static void refreshFolderFiles(Context context, String user, String password, String date,
                                          SwipeRefreshLayout swipeButton, FilesListFragment fragment) {
        String file_date = date;

        if (file_date == null) {
            SharedPreferences prefs = context.getSharedPreferences(
                    "user_pref", Context.MODE_PRIVATE);
            file_date = prefs.getString("files_last_refreshed", null);
        }

        refreshProjectsFromDate(context, user, password, file_date, swipeButton, fragment);
    }

    /**
     * Refreshes the projects.
     *
     * @param _context           context for lab.
     * @param _user              user for authentication.
     * @param _password          user for authentication.
     * @param _fromDate          last refreshed date.
     * @param swipeRefreshLayout refresh spinner instance so it can be stopped at the end.
     * @param fragment           the FilesListFragment instance so it cna be updated at the end.
     */

    public static void refreshProjectsFromDate(final Context _context, String _user, String _password,
                                               final String _fromDate, final SwipeRefreshLayout swipeRefreshLayout,
                                               final FilesListFragment fragment) {
        final Context context = _context;
        final String user = _user;
        final String password = _password;
        final String charset = "UTF-8";
        final String fromDate = _fromDate;


        final String url = "http://www.constructiononline.com:8080/" +
                "api/Projects?fromDate=" + fromDate;

        final AsyncTask<Void, Void, Void> downloadTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(user, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(user, charset),
                            URLEncoder.encode(password, charset));

                    // You put & when there is already paramaters and ? if this is the first param
                    URLConnection connection = new URL(url + "&" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();
                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            FolderFileLab lab = FolderFileLab.get(context);

                            FolderFile file = new FolderFile(object.getInt("ID"));
                            file.setIsProject(true);
                            file.setName(object.getString("NAME"));
                            file.setParent(-1);
                            file.setCreatedBy(object.getInt("CREATED_BY"));
                            file.setCreatorName(object.getString("CREATOR_NAME"));
                            file.setAddress(object.getString("ADDRESS"));
                            file.setCity(object.getString("CITY"));
                            file.setState(object.getString("STATE"));
                            file.setZip(object.getString("ZIP"));
                            file.setProjectNumber(object.getString("PROJECT_NUMBER"));
                            file.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            file.setLastModifiedName(object.getString("LMOD_NAME"));
                            file.setProjectType(object.getString("PROJECT_TYPE"));
                            file.setContactId(object.getInt("CON_ID"));
                            file.setContactName(object.getString("CON_NAME"));
                            file.setIsArchived(object.getBoolean("IS_ARCHIVED"));
                            file.setIsCanadian(object.getBoolean("IS_CANADIAN"));
                            file.setCurrency(object.getInt("CURRENCY"));
                            file.setIsTemplate(object.getBoolean("IS_TEMPLATE"));
                            file.setCountrySetting(object.getInt("COUNTRY_SETTING"));
                            file.setCustomColor(object.getString("CUSTOM_COLOR"));
                            file.setCompanyName(object.getString("COMPANY_NAME"));
                            file.setIsInactive(object.getBoolean("IS_INACTIVE"));
//                            file.setLat(object.getDouble("LAT"));
//                            file.setLon(object.getDouble("LON"));
                            file.setScopeOfWork(object.getString("SCOPE_OF_WORK"));
                            file.setIsUngrouped(object.getBoolean("IS_UNGROUPED"));
                            file.setTotalTime(object.getInt("TOTAL_TIME"));
                            file.setTotalMoney(object.getInt("TOTAL_MONEY"));
                            file.setProjectNotes(object.getString("PROJECT_NOTES"));
                            file.setNumberFolders(object.getInt("NUMBERFOLDERS"));
                            file.setNumberImages(object.getInt("NUMBERIMAGES"));
                            file.setNumberAlbums(object.getInt("NUMBERALBUMS"));
                            file.setNumberFiles(object.getInt("NUMBERFILES"));
                            file.setNumberComments(object.getInt("NUMBEROFCOMMENTS"));


                            String modifiedDateStr = object.getString("LMOD");
                            String createdDateStr = object.getString("DATE_CREATED");


                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");
                            try {
                                Date birthDate = df.parse(modifiedDateStr);
                                file.setModifiedDate(birthDate);

                                Date createdDate = df.parse(createdDateStr);
                                file.setDateCreated(createdDate);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            FolderFile testExistanceFile = lab.getFolderFile(file.getId());

                            if (testExistanceFile == null) {
                                // Child and stuff
                                lab.addFolderFile(file);
                            } else {
                                lab.deleteFolderFile(testExistanceFile);
                                lab.addFolderFile(file);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;


            }

            @Override
            public void onPostExecute(Void v) {
                deletedProjectsFromDate(context, user, password, fromDate,
                        swipeRefreshLayout, fragment);
            }
        };
        downloadTask.execute();
    }

    /**
     * Checks to see if any projects have been deleted.
     *
     * @param _context            context for lab.
     * @param _user               user for authentication.
     * @param _password           user for authentication.
     * @param _fromDate           last refreshed date.
     * @param _swipeRefreshLayout refresh spinner instance so it can be stopped at the end.
     * @param fragment            the FilesListFragment instance so it cna be updated at the end.
     */

    public static void deletedProjectsFromDate(Context _context, String _user, String _password,
                                               String _fromDate, SwipeRefreshLayout _swipeRefreshLayout,
                                               final FilesListFragment fragment) {
        final Context context = _context;
        final String user = _user;
        final String password = _password;
        final String charset = "UTF-8";
        final String fromDate = _fromDate;


        final SwipeRefreshLayout swipeRefreshLayout = _swipeRefreshLayout;
        final String url = "http://www.constructiononline.com:8080/" +
                "api/DeletedProjects?fromDate=" + fromDate;

        AsyncTask<Void, Void, List<FolderFile>> downloadTask = new AsyncTask<Void, Void, List<FolderFile>>() {
            private int position;


            @Override
            protected List<FolderFile> doInBackground(Void... params) {


                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(user, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(user, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "&" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();
                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            FolderFileLab lab = FolderFileLab.get(context);
                            FolderFile file = new FolderFile(object.getInt("ID"));
                            FolderFile testExistance = lab.getFolderFile(file.getId());

                            if (testExistance != null) {
                                lab.deleteFolderFile(testExistance);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<FolderFile> result) {
                refreshFilesFromDate(context, user, password,
                        fromDate, swipeRefreshLayout, fragment);
            }
        };

        downloadTask.execute();
    }

    /**
     * Refreshes the Files.
     *
     * @param _context            context for lab.
     * @param _user               user for authentication.
     * @param _password           user for authentication.
     * @param _fromDate           last refreshed date.
     * @param _swipeRefreshLayout refresh spinner instance so it can be stopped at the end.
     * @param fragment            the FilesListFragment instance so it cna be updated at the end.
     */

    public static void refreshFilesFromDate(Context _context, String _user, String _password,
                                            String _fromDate, SwipeRefreshLayout _swipeRefreshLayout,
                                            final FilesListFragment fragment) {
        final Context context = _context;
        final String user = _user;
        final String password = _password;
        final String charset = "UTF-8";
        final String fromDate = _fromDate;


        final SwipeRefreshLayout swipeRefreshLayout = _swipeRefreshLayout;
        final String url = "http://www.constructiononline.com:8080/" +
                "api/Projects?fromDate=" + fromDate;

        AsyncTask<Void, Void, List<FolderFile>> downloadTask = new AsyncTask<Void, Void, List<FolderFile>>() {
            private int position;


            @Override
            protected List<FolderFile> doInBackground(Void... params) {


                try {
                    Authenticator.setDefault(new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(user, password.toCharArray());
                        }
                    });

                    String query = String.format("username=%s&password=%s",
                            URLEncoder.encode(user, charset),
                            URLEncoder.encode(password, charset));

                    URLConnection connection = new URL(url + "&" + query).openConnection();
                    connection.setRequestProperty("Accept-Charset", charset);
                    InputStream response = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response));
                    String line;
                    StringBuilder result = new StringBuilder();

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    reader.close();
                    String output = result.toString();
                    try {
                        JSONArray jsonArray = new JSONArray(output);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            FolderFileLab lab = FolderFileLab.get(context);
                            FolderFile file = new FolderFile(object.getInt("ID"));
                            file.setProjectId(object.getInt("PRJ_ID"));

                            if (object.isNull("PARENT_ID")) {
                                file.setParent(file.getProjectId());
                            } else {
                                file.setParent(object.getInt("PARENT_ID"));
                            }
                            file.setName(object.getString("NAME"));

                            file.setExt(object.getString("EXT"));
                            file.setSize(object.getInt("SIZE"));
                            file.setType(object.getString("TYPE"));
                            file.setParentName(object.getString("PARENT_NAME"));

                            file.setProjectName(object.getString("PROJECT_NAME"));
                            file.setContactId(object.getInt("CON_ID"));
                            file.setContactName(object.getString("CON_NAME"));
                            file.setTinyUrl(object.getString("TINYURL"));
                            file.setMd5(object.getString("MD5"));
                            file.setIsDeleted(object.getBoolean("DELETED"));
                            file.setIsAlbum(object.getBoolean("ISALBUM"));
                            file.setLastModifiedContactId(object.getInt("LMOD_CON_ID"));
                            file.setLastModifiedName(object.getString("LMOD_CONTACT"));
                            file.setIsPhoto(object.getBoolean("ISIMAGE"));
                            file.setNumberOfChildren(object.getInt("NUMBEROFCHILDREN"));
                            file.setIsFolder(object.getBoolean("ISFOLDER"));
                            file.setNumberComments(object.getInt("NUMBEROFCOMMENTS"));
                            file.setCreatedBy(object.getInt("CREATED_BY"));
                            file.setCreatorName(object.getString("CREATOR"));
                            file.setCaption(object.getString("CAPTION"));
                            file.setFullName(object.getString("FULLNAME"));
                            file.setIsRecentAttachment(object.getBoolean("ISRECENTATTACHMENT"));
                            file.setIsPermAttachment(object.getBoolean("ISPERMATTACHMENT"));
                            file.setIsPrivate(object.getBoolean("ISPRIVATE"));
                            file.setIsClientFolder(object.getBoolean("IS_CLIENTFOLDER"));
                            file.setIsClientUploadFolder(object.getBoolean("IS_CLIENT_UPLOAD_FOLDER"));
                            file.setIsSubConFolder(object.getBoolean("IS_SUBCONFOLDER"));
                            //file.setIsSharedWithClients(object.getBoolean("SHARED_WITH_CLIENTS"));
                            //file.setSharedWithSubs(object.getBoolean("SHARED_WITH_SUBS"));

                            String lastAccessedStr = object.getString("LACCESS");
                            String dateTakenStr = object.getString("DATE_TAKEN");
                            String modifiedDateStr = object.getString("LMOD");
                            String createdDateStr = object.getString("DATE_CREATED");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSS");

                            try {
                                Date birthDate = df.parse(modifiedDateStr);
                                file.setModifiedDate(birthDate);

                                Date createdDate = df.parse(createdDateStr);
                                file.setDateCreated(createdDate);


                                if (!lastAccessedStr.equals("null")) {
                                    Date lastAccessed = df.parse(lastAccessedStr);
                                    file.setLastAccessed(lastAccessed);
                                }

                                if (!dateTakenStr.equals("null")) {
                                    Date dateTaken = df.parse(dateTakenStr);
                                    file.setDateTaken(dateTaken);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            FolderFile testExistance = lab.getFolderFile(file.getId());

                            if (testExistance == null) {
                                lab.addFolderFile(file);
                            } else {
                                lab.deleteFolderFile(lab.getFolderFile(file.getId()));
                                lab.addFolderFile(file);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(List<FolderFile> result) {
                // End refresh
                swipeRefreshLayout.setRefreshing(false);
                fragment.updateUI();


            }
        };
        downloadTask.execute();
    }


}
