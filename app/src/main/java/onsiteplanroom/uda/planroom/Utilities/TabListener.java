package onsiteplanroom.uda.planroom.Utilities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import onsiteplanroom.uda.planroom.R;

/**
 * Created by Harry on 1/20/2016.
 * This is a class that implements the ActionBar.TabListener interface. I use this class for
 * refactoring purposes to make my ToDosListActivity class cleaner.
 */
public class TabListener implements ActionBar.TabListener {

    /**
     * Fields
     */
    android.support.v4.app.Fragment fragment;

    /**
     * Override Methods
     */

    @Override
    public void onTabSelected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
        ft.replace(R.id.frame_layout_container, fragment);
    }

    @Override
    public void onTabUnselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
        ft.remove(fragment);
    }

    @Override
    public void onTabReselected(android.support.v7.app.ActionBar.Tab tab, FragmentTransaction ft) {
        //
    }

    /**
     * Utility Methods
     */

    public TabListener(android.support.v4.app.Fragment _fragment) {
        fragment = _fragment;
    }

}
