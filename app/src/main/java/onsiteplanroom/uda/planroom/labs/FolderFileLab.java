package onsiteplanroom.uda.planroom.labs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.FolderFile;
import onsiteplanroom.uda.planroom.database.ItemBaseHelper;
import onsiteplanroom.uda.planroom.database.ItemCursorWrapper;
import onsiteplanroom.uda.planroom.database.ItemDbSchema;

/**
 * Created by Harry on 2/3/2016.
 * This class is a SINGLETON
 * This class is used to interact with the FolderFile object and the database.
 * This class contains the add, update, and delete operations  for FolderFiles.
 * This class also contains the current parent for the file system.
 */

public class FolderFileLab {

    // ******
    // FIELDS
    // ******

    private static FolderFileLab sFolderFileLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;
    private Integer parentCurrent;
    private static Integer mNewItemToRename;
    private HashMap<Integer, Bitmap> mBitmapHashMap;
    private static List<FolderFile> sFolderFiles;

    // ***********
    // Constructor
    // ***********


    private FolderFileLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new ItemBaseHelper(mContext)
                .getWritableDatabase();
    }

    //**********
    // Methods
    //**********


    /** Called when the singleton is needed to call static methods from.
     * @return the FolderFileLab singleton
     */

    public static FolderFileLab get(Context context) {
        if (sFolderFileLab == null) {
            sFolderFileLab = new FolderFileLab(context);
        }
        return sFolderFileLab;
    }

    /** Receive all FolderFiles.
     * @return all the folder files that are in the database
     */

    public List<FolderFile> getFolderFiles() {
        List<FolderFile> files = new ArrayList<>();

        ItemCursorWrapper cursor = queryFolderFiles(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                files.add(cursor.getFolderFile());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        sFolderFiles = files;
        return files;
    }

    /** Get a specific FolderFile.
     * @param id  ID of folderfile you want to retrieve
     * @return the folderfile you asked for in the database
     */

    public FolderFile getFolderFile(Integer id) {
        if (id == null) {
            return null;
        } else {
            ItemCursorWrapper cursor = queryFolderFiles(
                    ItemDbSchema.FolderFileTable.Cols.ID + "=?",
                    new String[]{id.toString()}
            );
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }
                cursor.moveToFirst();
                return cursor.getFolderFile();
            } finally {
                cursor.close();
            }
        }
    }

    /** Add a FolderFile to the database.
     * @param file file to add to database
     */

    public void addFolderFile(FolderFile file) {
        ContentValues values = getContentValues(file);
        mDatabase.insert(ItemDbSchema.FolderFileTable.NAME, null, values);
    }

    /** Deleted a aFolder from the database.
     * @param file file to delete in database
     */

    public void deleteFolderFile(FolderFile file) {
        String idString = file.getId().toString();
        mDatabase.delete(ItemDbSchema.FolderFileTable.NAME, "id_folderfile = ?",
                new String[]{idString});
    }

    /** Loops to find which FolderFiles have the isProject attribute and puts them in a Collection.
     * @return All the projects in the database.
     */

    public List<FolderFile> getProjects() {

        List<FolderFile> allFiles = getFolderFiles();
        List<FolderFile> projectFiles = new ArrayList<>();

        for (FolderFile file: allFiles) {
            if (file.isProject()) {
                projectFiles.add(file);
            }
        }
        return projectFiles;
    }

    /**
     * Drops the current SQLite FolderFile table.
     */

    public void dropFolderFileTable() {

        mDatabase.execSQL("DROP TABLE IF EXISTS " + ItemDbSchema.FolderFileTable.NAME);
    }

    /**
     * Initializes a new SQLite FolderFile table.
     */

    public void initializeFolderFileTable() {

        mDatabase.execSQL("create table " + ItemDbSchema.FolderFileTable.NAME
                        + "(" + "_id integer primary key autoincrement,"
                        + ItemDbSchema.FolderFileTable.Cols.ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.PARENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.CHILDREN + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_MODIFIED_DATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PROJECT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PHOTO + ","
                        + ItemDbSchema.FolderFileTable.Cols.FILE_PATH + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_VIDEO + ","
                        + ItemDbSchema.FolderFileTable.Cols.CREATED_BY + ","
                        + ItemDbSchema.FolderFileTable.Cols.CREATOR_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.ADDRESS + ","
                        + ItemDbSchema.FolderFileTable.Cols.CITY + ","
                        + ItemDbSchema.FolderFileTable.Cols.STATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.ZIP + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_CON_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_TYPE + ","
                        + ItemDbSchema.FolderFileTable.Cols.CONTACT_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.CONTACT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_ARCHIVED + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CANADIAN + ","
                        + ItemDbSchema.FolderFileTable.Cols.CURRENCY + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_TEMPLATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.COUNTRY_SETTING + ","
                        + ItemDbSchema.FolderFileTable.Cols.CUSTOM_COLOR + ","
                        + ItemDbSchema.FolderFileTable.Cols.COMPANY_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.COMPANY_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_INACTIVE + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAT + ","
                        + ItemDbSchema.FolderFileTable.Cols.LON + ","
                        + ItemDbSchema.FolderFileTable.Cols.SCOPE_OF_WORK + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_UNGROUPED + ","
                        + ItemDbSchema.FolderFileTable.Cols.TOTAL_TIME + ","
                        + ItemDbSchema.FolderFileTable.Cols.TOTAL_MONEY + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NUMBER + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NOTES + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_FOLDERS + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_IMAGES + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_ALBUMS + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_FILES + ","
                        + ItemDbSchema.FolderFileTable.Cols.EXT + ","
                        + ItemDbSchema.FolderFileTable.Cols.SIZE + ","
                        + ItemDbSchema.FolderFileTable.Cols.TYPE + ","
                        + ItemDbSchema.FolderFileTable.Cols.PARENT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_ID + ","
                        + ItemDbSchema.FolderFileTable.Cols.PROJECT_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.TINY_URL + ","
                        + ItemDbSchema.FolderFileTable.Cols.MD5 + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_DELETED + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_ALBUM + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_CHILDREN + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.NUMBER_COMMENTS + ","
                        + ItemDbSchema.FolderFileTable.Cols.CAPTION + ","
                        + ItemDbSchema.FolderFileTable.Cols.FULL_NAME + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_RECENT_ATTACHMENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PERM_ATTACHMENT + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_PRIVATE + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_UPLOAD_FOLDER + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_CLIENTS + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_SUBS + ","
                        + ItemDbSchema.FolderFileTable.Cols.IS_OFFLINE + ","
                        + ItemDbSchema.FolderFileTable.Cols.LAST_ACCESSED + ","
                        + ItemDbSchema.FolderFileTable.Cols.DATE_CREATED + ","
                        + ItemDbSchema.FolderFileTable.Cols.DATE_TAKEN
                        + ")"
        );
    }

    /**
     * Recursive method to delete all the subfolders of the parent
     */

    public void deleteFolderWithChildren(Integer parent) {


        List<Integer> ids = sFolderFileLab.getFolderFile(parent).getChildren();
        if (ids != null) {
            for (Integer id : ids) {
                deleteFolderWithChildren(id);
                sFolderFileLab.deleteFolderFile(sFolderFileLab.getFolderFile(id));
            }
        }
    }


    /**
     * Updates a specified file in the database.
     *
     * @param file - The file to be updated.
     */

    public void updateFolderFile(FolderFile file) {
        String idString = file.getId().toString();
        ContentValues values = getContentValues(file);
        mDatabase.update(ItemDbSchema.FolderFileTable.NAME, values,
                ItemDbSchema.FolderFileTable.Cols.ID + "=?",
                new String[]{idString});
    }

    /**
     * SQLite stuff to store things in database
     *
     * @param folderFile FolderFile to get data from.
     * @return The values for everything stored in a HashMap
     */

    private static ContentValues getContentValues(FolderFile folderFile) {

        ContentValues values = new ContentValues();
        values.put(ItemDbSchema.FolderFileTable.Cols.ID, folderFile.getId().toString());
        values.put(ItemDbSchema.FolderFileTable.Cols.PARENT, folderFile.getParent().toString());

        List foldersChildren = folderFile.getChildren();

        String childrenString = "";

        if (foldersChildren != null) {
            for (int i = 0; i < foldersChildren.size(); i++) {
                childrenString += foldersChildren.get(i) + ",";
            }
        } else {
            childrenString = "";
        }
        values.put(ItemDbSchema.FolderFileTable.Cols.CHILDREN,childrenString);

        values.put(ItemDbSchema.FolderFileTable.Cols.FILE_NAME, folderFile.getName());
        values.put(ItemDbSchema.FolderFileTable.Cols.FILE_MODIFIED_DATE,
                folderFile.getModifiedDate().getTime());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_PROJECT, folderFile.isProject());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_PHOTO, folderFile.isPhoto());
        values.put(ItemDbSchema.FolderFileTable.Cols.FILE_PATH, folderFile.getFilePath());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_VIDEO, folderFile.isVideo());
        values.put(ItemDbSchema.FolderFileTable.Cols.CREATED_BY,folderFile.getCreatedBy());
        values.put(ItemDbSchema.FolderFileTable.Cols.CREATOR_NAME,folderFile.getCreatorName());
        values.put(ItemDbSchema.FolderFileTable.Cols.ADDRESS,folderFile.getAddress());
        values.put(ItemDbSchema.FolderFileTable.Cols.CITY,folderFile.getCity());
        values.put(ItemDbSchema.FolderFileTable.Cols.STATE,folderFile.getState());
        values.put(ItemDbSchema.FolderFileTable.Cols.ZIP,folderFile.getZip());
        values.put(ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_CON_ID
                ,folderFile.getLastModifiedContactId());
        values.put(ItemDbSchema.FolderFileTable.Cols.LAST_MODIFIED_NAME,
                folderFile.getLastModifiedName());
        values.put(ItemDbSchema.FolderFileTable.Cols.PROJECT_TYPE,folderFile.getProjectType());
        values.put(ItemDbSchema.FolderFileTable.Cols.CONTACT_ID,folderFile.getContactId());
        values.put(ItemDbSchema.FolderFileTable.Cols.CONTACT_NAME,folderFile.getContactName());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_ARCHIVED,folderFile.isArchived());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_CANADIAN,folderFile.isCanadian());
        values.put(ItemDbSchema.FolderFileTable.Cols.CURRENCY,folderFile.getCurrency());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_TEMPLATE,folderFile.isTemplate());
        values.put(ItemDbSchema.FolderFileTable.Cols.COUNTRY_SETTING,folderFile.getCountrySetting());
        values.put(ItemDbSchema.FolderFileTable.Cols.CUSTOM_COLOR,folderFile.getCustomColor());
        values.put(ItemDbSchema.FolderFileTable.Cols.COMPANY_ID,folderFile.getCompanyId());
        values.put(ItemDbSchema.FolderFileTable.Cols.COMPANY_NAME,folderFile.getCompanyName());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_INACTIVE,folderFile.isInactive());
        //values.put(ItemDbSchema.FolderFileTable.Cols.LAT,folderFile.getLat());
        //values.put(ItemDbSchema.FolderFileTable.Cols.LON,folderFile.getLon());
        values.put(ItemDbSchema.FolderFileTable.Cols.SCOPE_OF_WORK,folderFile.getScopeOfWork());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_UNGROUPED,folderFile.isUngrouped());
        values.put(ItemDbSchema.FolderFileTable.Cols.TOTAL_TIME,folderFile.getTotalTime());
        values.put(ItemDbSchema.FolderFileTable.Cols.TOTAL_MONEY,folderFile.getTotalMoney());
        values.put(ItemDbSchema.FolderFileTable.Cols.PROJECT_NUMBER,folderFile.getProjectNumber());
        values.put(ItemDbSchema.FolderFileTable.Cols.PROJECT_NOTES,folderFile.getProjectNotes());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_FOLDERS,folderFile.getNumberFolders());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_IMAGES,folderFile.getNumberImages());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_ALBUMS,folderFile.getNumberAlbums());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_FILES,folderFile.getNumberAlbums());
        values.put(ItemDbSchema.FolderFileTable.Cols.EXT,folderFile.getExt());
        values.put(ItemDbSchema.FolderFileTable.Cols.SIZE,folderFile.getSize());
        values.put(ItemDbSchema.FolderFileTable.Cols.TYPE,folderFile.getType());
        values.put(ItemDbSchema.FolderFileTable.Cols.PARENT_NAME,folderFile.getParentName());
        values.put(ItemDbSchema.FolderFileTable.Cols.PROJECT_ID,folderFile.getProjectId());
        values.put(ItemDbSchema.FolderFileTable.Cols.PROJECT_NAME,folderFile.getProjectName());
        values.put(ItemDbSchema.FolderFileTable.Cols.TINY_URL,folderFile.getTinyUrl());
        values.put(ItemDbSchema.FolderFileTable.Cols.MD5,folderFile.getMd5());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_DELETED,folderFile.isDeleted());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_ALBUM,folderFile.isAlbum());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_CHILDREN,
                folderFile.getNumberOfChildren());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_FOLDER,folderFile.isFolder());
        values.put(ItemDbSchema.FolderFileTable.Cols.NUMBER_COMMENTS,folderFile.getNumberComments());
        values.put(ItemDbSchema.FolderFileTable.Cols.CAPTION,folderFile.getCaption());
        values.put(ItemDbSchema.FolderFileTable.Cols.FULL_NAME,folderFile.getFullName());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_RECENT_ATTACHMENT,
                folderFile.isRecentAttachment());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_PERM_ATTACHMENT,
                folderFile.isPermAttachment());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_PRIVATE,folderFile.isPrivate());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_FOLDER,folderFile.isClientFolder());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_CLIENT_UPLOAD_FOLDER,
                folderFile.isClientUploadFolder());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_CLIENTS,
                folderFile.isSharedWithClients());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_SHARED_WITH_SUBS,
                folderFile.isSharedWithSubs());
        values.put(ItemDbSchema.FolderFileTable.Cols.IS_OFFLINE,folderFile.isOffline());
        values.put(ItemDbSchema.FolderFileTable.Cols.DATE_CREATED,folderFile.getDateCreated().getTime());
        values.put(ItemDbSchema.FolderFileTable.Cols.LAST_ACCESSED,folderFile.getLastAccessed().getTime());
        values.put(ItemDbSchema.FolderFileTable.Cols.DATE_TAKEN,folderFile.getDateTaken().getTime());


        return values;
    }

    /**
     * Database method that query the FolderFiles into the database.
     */

    private ItemCursorWrapper queryFolderFiles(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                ItemDbSchema.FolderFileTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new ItemCursorWrapper(cursor);
    }

    /** Receives the file for a picture.
     * @param folderFile - the picture that you want to find the file for.
     * @return the file for the picture specified.
     */

    public File getPhotoFile(FolderFile folderFile) {
        File externalFilesDir = mContext
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            return null;
        }
        return new File(externalFilesDir, folderFile.getPhotoFileName());
    }

    /** Receives the file for a video.
     * @param folderFile - the video that you want to find the file for.
     * @return the file for the video specified.
     */

    public File getVideoFile(FolderFile folderFile) {
        File externalFilesDir = mContext
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir == null) {
            return null;
        }
        return new File(externalFilesDir, folderFile.getVideoFileName());
    }

    /**
     * Points away from map and calls garbage collection.
     */

    public void clearMapFromMemory() {
        mBitmapHashMap = null;
        System.gc();
    }

    /**
     * Called after taking photo/video or called after uploading photo/video
     *
     * @param itemId item to rename
     */

    public void setRename(Integer itemId) {
        mNewItemToRename = itemId;
    }

    /** Called when the user needs to know what FolderFile needs renaming.
     * @return id that needs renaming
     */

    public Integer getRename() {
        return mNewItemToRename;
    }

    /** Determines if a FolderFile has been designated as the one that needs renaming
     * after taking a photo/video
     * @return used to determine if there is a file that needs renaming.
     */

    public boolean shouldRename() {
        for (FolderFile folderFile : this.getFolderFiles()) {
            if (folderFile.getId().equals(mNewItemToRename)) {
                return true;
            }
        }
        return false;
    }

    /** Receive the parent for the current directory.
     * @return the id of the folder/project that is currently open. If this is null then it is in
     * the top directory.
     */

    public Integer getParentCurrent() {
        return parentCurrent;
    }

    /** Set the current parent.
     * @param parentCurrent - the id you want to set the parent of the folder/project that is
     *                      currently being displayed.
     */

    public void setParentCurrent(Integer parentCurrent) {
        this.parentCurrent = parentCurrent;
    }
}
