package onsiteplanroom.uda.planroom.labs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import onsiteplanroom.uda.planroom.Objects.Contact;
import onsiteplanroom.uda.planroom.database.ItemBaseHelper;
import onsiteplanroom.uda.planroom.database.ItemCursorWrapper;
import onsiteplanroom.uda.planroom.database.ItemDbSchema;

/**
 * Created by Harry on 2/3/2016.
 * This class is what is a SINGLETON.
 * Used to interact between database and Contact class.
 * This class contains the add, delete, and update operations for a contact object.
 */
public class ContactLab {

    // ******
    // FIELDS
    // ******

    private static ContactLab sContactLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;


    // ***********
    // Constructor
    // ***********

    private ContactLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new ItemBaseHelper(mContext)
                .getWritableDatabase();
    }

    //**********
    // Methods
    //**********

    /** Use when the singleton is needed to class static methods from.
     * @param context The current context.
     * @return The singleton for the ContactLab class.
     */

    public static ContactLab get(Context context) {
        if (sContactLab == null) {
            sContactLab = new ContactLab(context);
        }
        return sContactLab;
    }

    /** Use when you need all Contacts.
     * @return Every contact in the database as a List.
     */

    public List<Contact> getContacts() {
        List<Contact> contacts = new ArrayList<>();

        ItemCursorWrapper cursor = queryContacts(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                contacts.add(cursor.getContact());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return contacts;
    }

    /** Use when you need a specific contact and you have the id
     * @param id The id for the contact that is needed.
     * @return The Contact with the specified id.
     */

    public Contact getContact(Integer id) {
        ItemCursorWrapper cursor = queryContacts(
                ItemDbSchema.ContactTable.Cols.ID + "=?",
                new String[]{id.toString()}
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getContact();
        } finally {
            cursor.close();
        }
    }

    /** Add a contact to the database.
     * @param contact The contact that will be added.
     */

    public void addContact(Contact contact) {
        ContentValues values = getContentValues(contact);
        mDatabase.insert(ItemDbSchema.ContactTable.NAME, null, values);
    }

    /** Delete a contact from the database.
     * @param contact The contact that will be deleted.
     */

    public void deleteContact(Contact contact) {
        String idString = contact.getId().toString();
        mDatabase.delete(ItemDbSchema.ContactTable.NAME, "id_contact = ?",
                new String[]{idString});
    }

    /** Update a contac that is already in the database.
     * @param contact The contact that will be updated
     */

    public void updateContact(Contact contact) {
        String idString = contact.getId().toString();
        ContentValues values = getContentValues(contact);

        mDatabase.update(ItemDbSchema.ContactTable.NAME, values,
                ItemDbSchema.ContactTable.Cols.ID + "=?",
                new String[]{idString});
    }

    /** Addes all the data for a specific contact into a hashmap.
     * @param contact the contact whose data is needed.
     * @return a Hashmap of data for the contact.
     */

    private static ContentValues getContentValues(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(ItemDbSchema.ContactTable.Cols.ID, contact.getId().toString());
        values.put(ItemDbSchema.ContactTable.Cols.FIRST_NAME, contact.getFirstName());
        values.put(ItemDbSchema.ContactTable.Cols.LAST_NAME, contact.getLastName());
        values.put(ItemDbSchema.ContactTable.Cols.ROLE, contact.getRole());
        values.put(ItemDbSchema.ContactTable.Cols.CONTACT_MODIFIED_DATE,
                contact.getDateModified().getTime());
        values.put(ItemDbSchema.ContactTable.Cols.EMAIL, contact.getEmail());
        values.put(ItemDbSchema.ContactTable.Cols.ACCOUNT, contact.getAccount());
        values.put(ItemDbSchema.ContactTable.Cols.DISPLAY_NAME, contact.getDisplayName());
        values.put(ItemDbSchema.ContactTable.Cols.COMPANY, contact.getCompany());
        values.put(ItemDbSchema.ContactTable.Cols.ADDRESS, contact.getAddress());
        values.put(ItemDbSchema.ContactTable.Cols.CITY, contact.getCity());
        values.put(ItemDbSchema.ContactTable.Cols.STATE, contact.getState());
        values.put(ItemDbSchema.ContactTable.Cols.ZIP, contact.getZip());
        values.put(ItemDbSchema.ContactTable.Cols.PHONE, contact.getPhone());
        values.put(ItemDbSchema.ContactTable.Cols.MOBILE_PHONE, contact.getMobilePhone());
        values.put(ItemDbSchema.ContactTable.Cols.MOBILE_PROVIDER, contact.getMobileProvider());
        values.put(ItemDbSchema.ContactTable.Cols.FAX, contact.getFax());
        values.put(ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_CONTACT_ID,
                contact.getLastModifiedContactId());
        values.put(ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_NAME, contact.getLastModifiedname());
        values.put(ItemDbSchema.ContactTable.Cols.IS_COMPANY, contact.isCompany());
        values.put(ItemDbSchema.ContactTable.Cols.MIDDLE_NAME, contact.getMiddleName());
        values.put(ItemDbSchema.ContactTable.Cols.WEBSITE, contact.getWebsite());
        values.put(ItemDbSchema.ContactTable.Cols.SUMMARY, contact.getSummary());
        values.put(ItemDbSchema.ContactTable.Cols.EXPERIENCE, contact.getExperience());
        values.put(ItemDbSchema.ContactTable.Cols.CREATOR_ID, contact.getCreatorId());
        values.put(ItemDbSchema.ContactTable.Cols.CREATOR_NAME, contact.getCreatorName());
        values.put(ItemDbSchema.ContactTable.Cols.TIMEZONE_OFFSET, contact.getTimezoneOffset());
        values.put(ItemDbSchema.ContactTable.Cols.AUTO_TIME_ZONE, contact.isAutoTimezone());
        values.put(ItemDbSchema.ContactTable.Cols.COMPANY_ID, contact.getCompanyId());
        values.put(ItemDbSchema.ContactTable.Cols.COMPANY_NAME, contact.getCompanyName());
        values.put(ItemDbSchema.ContactTable.Cols.COUNTRY_SETTING, contact.getCountrySetting());
        values.put(ItemDbSchema.ContactTable.Cols.LANGUAGE_SETTING, contact.getLanguageSetting());
        values.put(ItemDbSchema.ContactTable.Cols.LAT, contact.getLat());
        values.put(ItemDbSchema.ContactTable.Cols.LON, contact.getLon());
        values.put(ItemDbSchema.ContactTable.Cols.COMPANY_PERMSSIONS,
                contact.getCompanyPermissions());
        values.put(ItemDbSchema.ContactTable.Cols.IS_COMPANY_ADMIN, contact.getCompanyPermissions());
        values.put(ItemDbSchema.ContactTable.Cols.PREMIUM, contact.getPremium());
        values.put(ItemDbSchema.ContactTable.Cols.MOBILE_TOKENS, contact.getMobileTokens());
        values.put(ItemDbSchema.ContactTable.Cols.SUBSCRIPTION, contact.getSubscription());
        values.put(ItemDbSchema.ContactTable.Cols.PREMIUM_TYPE, contact.getPremiumType());
        values.put(ItemDbSchema.ContactTable.Cols.FIELD, contact.getField());


        return values;
    }

    /**
     * Drops the current contact table in the database.
     */

    public void dropContactTable() {
        mDatabase.execSQL("DROP TABLE IF EXISTS " + ItemDbSchema.ContactTable.NAME);
    }

    /** Querys the contacts into the database.
     * @param whereClause
     * @param whereArgs
     * @return wrapper for an item cursor.
     */

    private ItemCursorWrapper queryContacts(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                ItemDbSchema.ContactTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new ItemCursorWrapper(cursor);
    }

    /**
     * Create a contact table in the database.
     */

    public void initializeContactTable() {
        mDatabase.execSQL("create table " + ItemDbSchema.ContactTable.NAME
                        + "(" + "_id integer primary key autoincrement,"
                        + ItemDbSchema.ContactTable.Cols.ID + ","
                        + ItemDbSchema.ContactTable.Cols.FIRST_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.ROLE + ","
                        + ItemDbSchema.ContactTable.Cols.CONTACT_MODIFIED_DATE + ","
                        + ItemDbSchema.ContactTable.Cols.EMAIL + ","
                        + ItemDbSchema.ContactTable.Cols.ACCOUNT + ","
                        + ItemDbSchema.ContactTable.Cols.DISPLAY_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY + ","
                        + ItemDbSchema.ContactTable.Cols.ADDRESS + ","
                        + ItemDbSchema.ContactTable.Cols.CITY + ","
                        + ItemDbSchema.ContactTable.Cols.STATE + ","
                        + ItemDbSchema.ContactTable.Cols.ZIP + ","
                        + ItemDbSchema.ContactTable.Cols.PHONE + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_PHONE + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_PROVIDER + ","
                        + ItemDbSchema.ContactTable.Cols.FAX + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_CONTACT_ID + ","
                        + ItemDbSchema.ContactTable.Cols.LAST_MODIFIED_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.IS_COMPANY + ","
                        + ItemDbSchema.ContactTable.Cols.MIDDLE_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.WEBSITE + ","
                        + ItemDbSchema.ContactTable.Cols.SUMMARY + ","
                        + ItemDbSchema.ContactTable.Cols.EXPERIENCE + ","
                        + ItemDbSchema.ContactTable.Cols.CREATOR_ID + ","
                        + ItemDbSchema.ContactTable.Cols.CREATOR_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.TIMEZONE_OFFSET + ","
                        + ItemDbSchema.ContactTable.Cols.AUTO_TIME_ZONE + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_ID + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_NAME + ","
                        + ItemDbSchema.ContactTable.Cols.COUNTRY_SETTING + ","
                        + ItemDbSchema.ContactTable.Cols.LANGUAGE_SETTING + ","
                        + ItemDbSchema.ContactTable.Cols.LAT + ","
                        + ItemDbSchema.ContactTable.Cols.LON + ","
                        + ItemDbSchema.ContactTable.Cols.COMPANY_PERMSSIONS + ","
                        + ItemDbSchema.ContactTable.Cols.IS_COMPANY_ADMIN + ","
                        + ItemDbSchema.ContactTable.Cols.PREMIUM + ","
                        + ItemDbSchema.ContactTable.Cols.MOBILE_TOKENS + ","
                        + ItemDbSchema.ContactTable.Cols.SUBSCRIPTION + ","
                        + ItemDbSchema.ContactTable.Cols.PREMIUM_TYPE + ","
                        + ItemDbSchema.ContactTable.Cols.FIELD
                        + ")"
        );
    }

}
